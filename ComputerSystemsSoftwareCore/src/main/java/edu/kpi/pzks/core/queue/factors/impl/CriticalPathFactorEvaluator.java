package edu.kpi.pzks.core.queue.factors.impl;

import edu.kpi.pzks.core.model.Link;
import edu.kpi.pzks.core.model.Node;
import edu.kpi.pzks.core.queue.factors.AbstractFactorEvaluator;
import java.util.Collection;

/**
 * Quick hack for accessing function getCriticalPathWeightForGraph
 * @author Aloren
 */
public class CriticalPathFactorEvaluator extends AbstractFactorEvaluator {

    public CriticalPathFactorEvaluator(Collection<Node> nodes, Collection<Link> links) {
        super(nodes, links);
    }

    public int getCriticalPath() {
        return getCriticalPathWeightForGraph();
    }

    @Override
    public double evaluateFactorForNode(Node node) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean hasNaturalOrder() {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}