package edu.kpi.pzks.core.queue.factors.impl;

import edu.kpi.pzks.core.model.Link;
import edu.kpi.pzks.core.model.Node;
import edu.kpi.pzks.core.queue.factors.AbstractFactorEvaluator;
import java.util.Collection;

/**
 * Variant 8 
 * Part 1
 * 
 * У порядку зростання критичного шляху по кількості вершин від
 * початку графа, 
 * а при рівних значеннях – в порядку спадання ваги вершин
 *
 * @author Aloren
 */
public class CriticalPathByNumberOfNodesAscFactorEvaluatorImpl extends AbstractFactorEvaluator {

    public CriticalPathByNumberOfNodesAscFactorEvaluatorImpl(Collection<Node> nodes, Collection<Link> links) {
        super(nodes, links);
    }


    @Override
    public double evaluateFactorForNode(Node node) {
        return getCriticalPathNumberFromNodeToTop(node);
    }

    @Override
    public boolean hasNaturalOrder() {
        return true;
    }
}
