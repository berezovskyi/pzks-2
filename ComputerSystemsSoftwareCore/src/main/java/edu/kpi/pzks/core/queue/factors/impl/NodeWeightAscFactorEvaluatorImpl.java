package edu.kpi.pzks.core.queue.factors.impl;

import edu.kpi.pzks.core.model.Node;
import edu.kpi.pzks.core.queue.factors.FactorEvaluator;


/**
 * Метод формирования очередей №15
 *
 * @author Snoop
 */
public class NodeWeightAscFactorEvaluatorImpl implements FactorEvaluator {

    @Override
    public double evaluateFactorForNode(Node node) {
        return node.getWeight();
    }

    @Override
    public boolean hasNaturalOrder() {
        return true; // в порядке возростания
    }
}
