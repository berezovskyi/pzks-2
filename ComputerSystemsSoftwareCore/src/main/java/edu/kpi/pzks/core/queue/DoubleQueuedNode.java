package edu.kpi.pzks.core.queue;

import edu.kpi.pzks.core.model.Node;

/**
 * @author smarx
 */
public class DoubleQueuedNode extends QueuedNode {

    protected double queueingFactorSecond;

    public DoubleQueuedNode(Node node, double queueingFactor, double queueingFactorSecond) {
        super(node, queueingFactor);
        this.queueingFactorSecond = queueingFactorSecond;
    }

    @Override
    public String toString() {
        return String.format("%d (%.4f; %.4f)", node.getId(), queueingFactor, queueingFactorSecond);
    }
}
