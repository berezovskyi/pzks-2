package edu.kpi.pzks.core.queue.factors.impl;

import edu.kpi.pzks.core.model.Link;
import edu.kpi.pzks.core.model.Node;
import edu.kpi.pzks.core.queue.factors.AbstractFactorEvaluator;

import java.util.Collection;

/**
 * @author smarx
 */
public class CriticalPathByNumberOfNodesDescFactorEvaluatorImpl extends AbstractFactorEvaluator {
    public CriticalPathByNumberOfNodesDescFactorEvaluatorImpl(Collection<Node> nodes, Collection<Link> links) {
        super(nodes, links);
    }

    @Override
    public double evaluateFactorForNode(Node node) {
        return getCriticalPathNumberFromNodeToBottom(node);
    }

    @Override
    public boolean hasNaturalOrder() {
        return false;
    }
}
