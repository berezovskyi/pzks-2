package edu.kpi.pzks.core.assign;

import edu.kpi.pzks.core.model.Link;
import java.util.*;

/**
 *
 * @author Aloren
 */
public class Processor {

    private final int id;
    private int freeChannels;
    private boolean isFree = true;
    private Map<Processor, List<Processor>> routingTable = new HashMap<>();
    private Map<Processor, Link> neighbourProcessors = new HashMap<>();
    private int performance;

    Processor(int id, int performance, int freeChannels) {
        this.id = id;
        this.performance = performance;
        this.freeChannels = freeChannels;
    }

    public int getId() {
        return id;
    }

    public boolean hasFreeChannels() {
        return freeChannels > 0;
    }

    public int occupyChannel() {
        if (freeChannels > 0) {
            freeChannels--;
        } else {
            throw new RuntimeException("No free channels on this proc");
        }
        return freeChannels;
    }

    public void releaseChannel() {
        freeChannels++;
    }

    public Map<Processor, List<Processor>> getRoutingTable() {
        return routingTable;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Processor other = (Processor) obj;
        return this.id == other.id;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 61 * hash + this.id;
        return hash;
    }

    public boolean isFree() {
        return isFree;
    }

    public void setFree(boolean free) {
        isFree = free;
    }

    public void setState(ProcState state) {
        this.isFree = state.isFree();
        this.freeChannels = state.getFreeChannels();
    }

    public ProcState getState() {
        ProcState state = new ProcState();
        state.setFree(isFree);
        state.setFreeChannels(freeChannels);
        return state;
    }

    @Override
    public String toString() {
        return "P" + id;
    }

    public int getPerformance() {
        return performance;
    }

    public Link getLinkTo(Processor neighbour) {
        Link link = neighbourProcessors.get(neighbour);
        if (link == null) {
            throw new IllegalArgumentException(this.toString()+ ": no link defined for " + neighbour);
        }
        return link;
    }

    void addNeighbourProcessor(Processor to, Link link) {
        Link addedLink = this.neighbourProcessors.put(to, link);
        if (addedLink != null) {
            throw new IllegalArgumentException("Trying to add duplicate link for processor " + to);
        }
    }
}
