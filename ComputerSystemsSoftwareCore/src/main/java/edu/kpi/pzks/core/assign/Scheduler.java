package edu.kpi.pzks.core.assign;

import edu.kpi.pzks.core.model.Graph;
import edu.kpi.pzks.core.queue.Queue;

import java.util.List;
import java.util.Set;

/**
 * Author: snoop Date: 13.04.13 Time: 12:42
 */
public interface Scheduler {
    /**
     * load this scheduler with tasks
     * @param taskGraph the actual task graph.
     */
    void setTaskGraph(Graph taskGraph);

    /**
     * inform this scheduler about processors.
     * @param processors set of processors in the system.
     */
    void setProcessors(Set<Processor> processors);
    /**
     * Schedule first level tasks on the processors
     */
    void schedule(SimulationInfo simulationInfo);

    /**
     * Get scheduled events. Called after {@link #schedule(SimulationInfo simulationInfo)} is complete.
     * @return scheduled events
     */
    List<Executable> getEvents();

    /**
     * Schedule tasks during modelling according to the Queue   (see {@link #schedule(SimulationInfo simulationInfo)})
     * @param exec - tasks to schedule
     */
    void schedule(List<Executable> exec);
}
