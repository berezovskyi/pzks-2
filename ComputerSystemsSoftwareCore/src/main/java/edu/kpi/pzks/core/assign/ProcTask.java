package edu.kpi.pzks.core.assign;

import edu.kpi.pzks.core.queue.QueuedNode;

/**
 *
 * @author Aloren
 */
public class ProcTask extends AbstractExecutable implements Executable {

    private final QueuedNode queuedNode;

    public ProcTask(QueuedNode node) {
        this.queuedNode = node;
        executionTime = this.queuedNode.getNode().getWeight();
    }


    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append(this.queuedNode.getNode().getId());
        sb.append("(");
        sb.append(this.queuedNode.getNode().getWeight());
        sb.append(")");
        return sb.toString();
    }

    public QueuedNode getQueuedNode() {
        return queuedNode;
    }
}
