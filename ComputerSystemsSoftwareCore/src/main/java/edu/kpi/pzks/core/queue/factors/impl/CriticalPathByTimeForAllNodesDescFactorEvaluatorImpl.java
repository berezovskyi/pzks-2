package edu.kpi.pzks.core.queue.factors.impl;

import edu.kpi.pzks.core.model.Link;
import edu.kpi.pzks.core.model.Node;
import edu.kpi.pzks.core.queue.factors.AbstractFactorEvaluator;

import java.util.Collection;

/**
 * Метод формирования очередей №3
 *
 * @author Snoop
 */
public class CriticalPathByTimeForAllNodesDescFactorEvaluatorImpl extends AbstractFactorEvaluator {
    public CriticalPathByTimeForAllNodesDescFactorEvaluatorImpl(Collection<Node> nodes, Collection<Link> links) {
        super(nodes, links);
    }

    @Override
    public double evaluateFactorForNode(Node node) {
        return getCriticalPathWeightFromNodeToBottom(node);
    }

    @Override
    public boolean hasNaturalOrder() {
        return false; // в порядке убывания
    }
}
