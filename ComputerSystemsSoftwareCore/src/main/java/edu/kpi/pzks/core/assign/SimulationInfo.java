package edu.kpi.pzks.core.assign;

import edu.kpi.pzks.core.queue.Queue;

/**
 * Author: snoop Date: 13.04.13 Time: 22:28
 */
public class SimulationInfo {

    @Deprecated
    int processorPerformance;
    private int channelsCount;
    @Deprecated
    private int bandwidth;
    private LinkMode linkMode;
    private Queue queue;

    @Deprecated
    public SimulationInfo(int processorPerformance, int channelsCount, int bandwidth, LinkMode linkMode, Queue queue) {
        this.processorPerformance = processorPerformance;
        this.channelsCount = channelsCount;
        this.bandwidth = bandwidth;
        this.linkMode = linkMode;
        this.queue = queue;
    }

    public SimulationInfo(int channelsCount, LinkMode linkMode, Queue queue) {
        this.channelsCount = channelsCount;
        this.linkMode = linkMode;
        this.queue = queue;
    }

    public enum LinkMode {

        DUPLEX, HALF_DUPLEX
    }

    @Deprecated
    public int getProcessorPerformance() {
        return processorPerformance;
    }

    public int getChannelsCount() {
        return channelsCount;
    }

    @Deprecated
    public int getBandwidth() {
        return bandwidth;
    }

    public LinkMode getLinkMode() {
        return linkMode;
    }

    public Queue getQueue() {
        return queue;
    }
}