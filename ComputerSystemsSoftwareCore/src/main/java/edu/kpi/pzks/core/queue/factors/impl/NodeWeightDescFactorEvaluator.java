package edu.kpi.pzks.core.queue.factors.impl;

import edu.kpi.pzks.core.model.Node;
import edu.kpi.pzks.core.queue.factors.FactorEvaluator;

/**
 * Variant 8 
 * Part 2
 * 
 * У порядку зростання критичного шляху по кількості вершин від
 * початку графа, 
 * а при рівних значеннях – в порядку спадання ваги вершин
 *
 * @author Aloren
 */
public class NodeWeightDescFactorEvaluator implements FactorEvaluator {

    @Override
    public double evaluateFactorForNode(Node node) {
        return node.getWeight();
    }

    @Override
    public boolean hasNaturalOrder() {
        return false;
    }
    
}
