package edu.kpi.pzks.core.assign;

import edu.kpi.pzks.core.model.Graph;
import edu.kpi.pzks.core.model.Link;
import edu.kpi.pzks.core.model.Node;

import java.util.*;

/**
 * @author Aloren
 */
public class ProcessorSystem {

    Set<Processor> processors;
    private Graph taskGraph;
    private Graph systemGraph;
    public Simulation simulation = new Simulation();
    public SimulationInfo simulationInfo;
    public Scheduler scheduler;
    Statistics statistics;

    public ProcessorSystem(Graph systemGraph, Graph taskGraph) {
        this.taskGraph = taskGraph;
        this.systemGraph = systemGraph;
    }

    //Create processors and build routing tables for each of them.
    void initProcessors() {
        Set<Node> nodes = systemGraph.getNodes();
        this.processors = new HashSet<>(nodes.size());
        Map<Integer, Processor> processorMap = new HashMap<>();
        for (Node node : nodes) {
            int id = node.getId();
            int performance = node.getWeight();
            Processor processor = new Processor(id, performance, simulationInfo.getChannelsCount());
            processorMap.put(id, processor);
            processors.add(processor);
        }

        for (Link link : systemGraph.getLinks()) {
            Processor from = processorMap.get(link.getFromNode().getId());
            Processor to = processorMap.get(link.getToNode().getId());
            from.addNeighbourProcessor(to, link);
            to.addNeighbourProcessor(from, link);
            to.getRoutingTable().put(from, Arrays.asList(from));
            from.getRoutingTable().put(to, Arrays.asList(to));
        }

        for (Processor processor : processors) {
            //while not found routes to all processors
            int i = 1; //start from 1st step
            while (processor.getRoutingTable().size() != processors.size() - 1) {
                Map<Processor, List<Processor>> tempMap = new HashMap<>();
                for (Processor buddy : processor.getRoutingTable().keySet()) {
                    if (processor.getRoutingTable().get(buddy).size() != i) {
                        continue;
                    }
                    for (Processor buddysbuddy : buddy.getRoutingTable().keySet()) {
                        if (buddy.getRoutingTable().get(buddysbuddy).size() == 1) {
                            if (!processor.getRoutingTable().keySet().contains(buddysbuddy) && !processor
                                    .equals(buddysbuddy)) {
                                List<Processor> route = new LinkedList<>(processor.getRoutingTable().get(buddy));
                                route.add(buddysbuddy);
                                tempMap.put(buddysbuddy, route);
                            }
                        }
                    }
                }
                processor.getRoutingTable().putAll(tempMap);
                i++;
            }
        }
    }

    public class Simulation {

        private Scheduler scheduler;

        public void start() {
            initProcessors();
            this.scheduler = ProcessorSystem.this.scheduler;
            scheduler.setProcessors(processors);
            scheduler.setTaskGraph(taskGraph);
            scheduler.schedule(simulationInfo);

            List<Executable> waitingEvents = scheduler.getEvents();
            List<Executable> completedEvents = new LinkedList<>();
            List<Executable> runningEvents = new LinkedList<>();
            List<ProcTransfer> runningTransfers = new LinkedList<>();

            int time = 0; //time unit - 1 processor clock

            while (!waitingEvents.isEmpty() || !runningEvents.isEmpty()) {
                //run something
                for (Executable event : runningEvents) {
                    if (event instanceof ProcTask) {
                        final Processor processor = event.getProcessor();
                        final int performance = processor.getPerformance();
//                        System.out.println("Executing " + event + " on " + processor + " performance: " + performance);
                        event.exec(performance);
                    } else if (event instanceof ProcTransfer) {
                        ProcTransfer transfer = (ProcTransfer) event;
                        final int bandwidth = transfer.getProcessorLink().getWeight();
//                        System.out.println("Executing " + event + " on " + transfer.getLink() + " bandwidth: " + bandwidth);
                        event.exec(bandwidth);
                    }
                    //check if completed, possibly <0 (as we subtract >0 integer each time)
                    if (event.getExecLeft() <= 0) {
                        event.setEndTime(time);
                        completedEvents.add(event);
                        if (event instanceof ProcTransfer) {
                            ProcTransfer transfer = (ProcTransfer) event;
                            if (!transfer.isDuplex()) {
                                transfer.getProcessor().releaseChannel();
                                transfer.getToProcessor().releaseChannel();
                            }
                            runningTransfers.remove(event);
                        } else {
                            event.getProcessor().setFree(true);
                        }
                    }
                }
                runningEvents.removeAll(completedEvents);

                //schedule all possible tasks (if not scheduled already)
                List<Executable> eventsToSchedule = new LinkedList<>();
                for (Executable event : waitingEvents) {
                    if (event instanceof ProcTask && event.getProcessor() == null) {
                        boolean canSchedule = true;
                        for (Executable dependency : event.getDependsOnList()) {
                            if (!completedEvents.contains(dependency)) {
                                canSchedule = false;
                            }
                        }
                        if (canSchedule) {
                            eventsToSchedule.add(event);
                        }
                    }
                }
                scheduler.schedule(eventsToSchedule);
                //look through waiting list if we can start running anything
                for (Executable event : waitingEvents) {
                    boolean canStart = true;
                    for (Executable dependency : event.getDependsOnList()) {
                        if (!completedEvents.contains(dependency)) {
                            canStart = false;
                        }
                    }
                    if (canStart) {
                        if (event instanceof ProcTask) {
                            if (event.getProcessor() != null) {
                                event.getProcessor().setFree(false);
                                event.setStartTime(time);
                                runningEvents.add(event);
                            }
                        } else {
                            ProcTransfer transfer = (ProcTransfer) event;
                            //check if both processors have free channel
                            boolean hasFreeChannels =
                                    transfer.getProcessor().hasFreeChannels() && transfer.getToProcessor()
                                    .hasFreeChannels();
                            boolean isFree = true;
                            for (ProcTransfer conflict : runningTransfers) {
                                //check whether this link is occupied
                                if (conflict.getToProcessor().equals(transfer.getToProcessor()) && conflict
                                        .getProcessor().equals(transfer.getProcessor())) {
                                    isFree = false;
                                    break;
                                }
                                //check if someone is sending info in the opposite way (only for half duplex)
                                if (simulationInfo.getLinkMode() == SimulationInfo.LinkMode.HALF_DUPLEX) {
                                    if (conflict.getToProcessor().equals(transfer.getProcessor()) && conflict
                                            .getProcessor().equals(transfer.getToProcessor())) {
                                        isFree = false;
                                        break;
                                    }
                                } //if duplex can work with one channel - TODO ask Rusanova
                                else {
                                    if (conflict.getToProcessor().equals(transfer.getProcessor()) && conflict
                                            .getProcessor().equals(transfer.getToProcessor())) {
                                        transfer.setChannel(conflict.getChannel());
                                        transfer.setDuplex(true);
                                        break;
                                    }
                                }
                            }
                            if (isFree && hasFreeChannels || transfer.isDuplex()) {

                                if (!transfer.isDuplex() && hasFreeChannels) {
                                    int channel = transfer.getProcessor().occupyChannel();
                                    transfer.getToProcessor().occupyChannel();
                                    transfer.setChannel(channel);
                                }
                                transfer.setStartTime(time);
                                runningEvents.add(transfer);
                                runningTransfers.add(transfer);
                            }
                        }
                    }
                }
                waitingEvents.removeAll(runningEvents);
                time++;
            }

            statistics = new Statistics();
            statistics.events = completedEvents;
            statistics.totalExecutionTime = time;
            statistics.channels = simulationInfo.getChannelsCount();
            statistics.processors = new LinkedList<>(processors);
        }

        public Statistics collectStatistics() {
            return statistics;
        }
    }

    public class Statistics {

        public int totalExecutionTime;
        public List<Executable> events;
        public List<Processor> processors;
        public int channels;
    }
}
