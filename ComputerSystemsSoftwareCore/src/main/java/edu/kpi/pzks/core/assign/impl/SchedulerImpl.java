package edu.kpi.pzks.core.assign.impl;

import edu.kpi.pzks.core.assign.*;
import edu.kpi.pzks.core.model.Graph;
import edu.kpi.pzks.core.model.Link;
import edu.kpi.pzks.core.model.Node;
import edu.kpi.pzks.core.queue.QueuedNode;

import java.util.*;

/**
 * Extend this class and implement abstract methods according to your variant
 * Author: snoop Date: 13.04.13 Time: 15:27
 */
public abstract class SchedulerImpl implements Scheduler {

    protected Graph tasks;
    protected List<Processor> processors;
    protected List<Executable> events = new LinkedList<>();
    protected Collection<QueuedNode> orderedNodes;
    protected SimulationInfo simulationInfo;
    Map<Node, QueuedNode> nodesMap = new HashMap<>();
    Map<QueuedNode, ProcTask> nodesTasksMap = new HashMap<>();

    @Override
    public void setTaskGraph(Graph taskGraph) {
        this.tasks = taskGraph;
    }

    @Override
    public void setProcessors(Set<Processor> processors) {
        int procCount = processors.size();
        this.processors = new ArrayList<>(procCount);
        this.processors.addAll(processors);
    }

    /**
     * Schedule initial tasks on the processors Used 3rd algorithm - you should
     * not normally override this method according to your variant.
     */
    @Override
    public void schedule(SimulationInfo simulationInfo) {
        this.simulationInfo = simulationInfo;
        orderedNodes = simulationInfo.getQueue().evaluate();

        Collections.sort(processors, LinksCountComparator());

        int i = 0;

        for (QueuedNode qnode : orderedNodes) {
            nodesMap.put(qnode.getNode(), qnode);
            ProcTask task = new ProcTask(qnode);
            //schedule initial tasks
            if (qnode.getNode().getInputNodes().isEmpty()) {
                if(i < processors.size()) {
                    task.setProcessor(processors.get(i));
                    processors.get(i).setFree(false);
                    i++;
                }
            }
            events.add(task);
            nodesTasksMap.put(qnode, task);
        }

        for (Executable exec : events) {
            ProcTask task = (ProcTask) exec;
            for (Node node : task.getQueuedNode().getNode().getInputNodes()) {
                ProcTask dependency = nodesTasksMap.get(nodesMap.get(node));
                task.getDependsOnList().add(dependency);
            }
        }
    }

    @Override
    public List<Executable> getEvents() {
        return events;
    }

    @Override
    public void schedule(List<Executable> executables) {
        for (QueuedNode node : orderedNodes) {
            for (Executable exec : executables) {
                if (exec instanceof ProcTask && ((ProcTask) exec).getQueuedNode().equals(node)) {
                    schedule(exec);
                }
            }
        }
    }

    /**
     * Assign concrete task on the processor. Here you SHOULD implement logic
     * according to your variant
     *
     * @param exec task to assign
     */
    protected abstract void schedule(Executable exec);

    private Comparator<Processor> LinksCountComparator() {
        return new Comparator<Processor>() {
            @Override
            public int compare(Processor o1, Processor o2) {
                int o1Links = 0;
                int o2Links = 0;
                for (Processor p : o1.getRoutingTable().keySet()) {
                    if (o1.getRoutingTable().get(p).size() == 1) {
                        o1Links++;
                    }
                }
                for (Processor p : o2.getRoutingTable().keySet()) {
                    if (o2.getRoutingTable().get(p).size() == 1) {
                        o2Links++;
                    }
                }

                if (o1Links > o2Links) {
                    return -1;
                }
                return 1;
            }
        };
    }

    protected List<Executable> assignTaskOnProcessor(Executable task, Processor processor) {
        task.setProcessor(processor);
        processor.setFree(false);

        List<Executable> transfersToWait = new LinkedList<>();
        for (Executable dependentExec : task.getDependsOnList()) {
            Processor fromProc = dependentExec.getProcessor();
            Processor toProc = task.getProcessor();
            Link dataLink = null;
            if (dependentExec instanceof ProcTask) {
                for (Link l : tasks.getLinks()) {
                    if (nodesTasksMap.get(nodesMap.get(l.getFromNode())).equals(dependentExec)
                            && nodesTasksMap.get(nodesMap.get(l.getToNode())).equals(task)) {
                        dataLink = l;
                        break;
                    }
                }
            } else {
                dataLink = ((ProcTransfer) dependentExec).getLink();
            }

            Executable dependency = dependentExec;

            if (!fromProc.equals(toProc)) {

                Map<Processor, List<Processor>> routingTable = fromProc.getRoutingTable();
                List<Processor> route = routingTable.get(toProc);
                for (Processor nextProc : route) {
                    ProcTransfer transfer = new ProcTransfer(dataLink);
                    Link betweenProcLink = fromProc.getLinkTo(nextProc);
                    transfer.setProcessorLink(betweenProcLink);
                    transfer.setProcessor(fromProc);
                    transfer.setToProcessor(nextProc);
                    transfer.getDependsOnList().add(dependency);
                    fromProc = nextProc;
                    events.add(transfer);
                    dependency = transfer;
                    transfersToWait.add(transfer);
                }
            }
        }
        task.getDependsOnList().addAll(transfersToWait);
        return transfersToWait;
    }
}
