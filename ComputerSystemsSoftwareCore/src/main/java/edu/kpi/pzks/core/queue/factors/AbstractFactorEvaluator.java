package edu.kpi.pzks.core.queue.factors;

import edu.kpi.pzks.core.model.Link;
import edu.kpi.pzks.core.model.Node;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author smarx
 */
public abstract class AbstractFactorEvaluator implements FactorEvaluator {

    private Collection<Node> nodes;
    private Collection<Link> links;

    public AbstractFactorEvaluator(Collection<Node> nodes, Collection<Link> links) {
        this.nodes = nodes;
        this.links = links;
    }

    /**
     * Returns graph's critical path taking into account nodes' weights.
     *
     * @return
     */
    protected List<Node> getCriticalPathForGraph() {
        int criticalWeight = 0;
        List<Node> criticalPath = null;
        for (Node n : nodes) {
            if (n.getOutputNodes().isEmpty()) {
                List<Node> currentPath = findCriticalPathFromNodeToTop(n);
                int currentWeight = calculatePathWeight(currentPath);
                if (currentWeight > criticalWeight) {
                    criticalWeight = currentWeight;
                    criticalPath = currentPath;
                }
            }
        }
        return criticalPath;
    }

    /**
     * Returns graph's longest path (NOT taking into account nodes' weights).
     *
     * @return
     */
    protected List<Node> getLongestPathForGraph() {
        int numberOfNodes = 0;
        List<Node> criticalPath = null;
        for (Node n : nodes) {
            if (n.getOutputNodes().isEmpty()) {
                List<Node> currentPath = findLongestPathFromNodeToTop(n);
                int curNumberOfNodes = currentPath.size();
                if (curNumberOfNodes > numberOfNodes) {
                    numberOfNodes = curNumberOfNodes;
                    criticalPath = currentPath;
                }
            }
        }
        return criticalPath;
    }

    /**
     * Returns weight for graph's critical path.
     *
     * @return
     */
    protected int getCriticalPathWeightForGraph() {
        List<Node> criticalPathForGraph = getCriticalPathForGraph();
        return calculatePathWeight(criticalPathForGraph);
    }

    /**
     * Returns weight for critical path from node n to the bottom of the graph.
     *
     * @param n
     * @return
     */
    protected int getCriticalPathWeightFromNodeToBottom(Node n) {
        List<Node> criticalPath = findCriticalPathFromNodeToBottom(n);
        return calculatePathWeight(criticalPath);
    }

    /**
     * Returns weight for critical path from node n to the top of the graph.
     *
     * @param n
     * @return
     */
    protected int getCriticalPathWeightFromNodeToTop(Node n) {
        List<Node> criticalPath = findCriticalPathFromNodeToTop(n);
        return calculatePathWeight(criticalPath);
    }

    /**
     * Returns normal critical path weight from node n to the bottom of the
     * graph.
     *
     * @param n
     * @return
     */
    protected double getNormalCriticalPathWeightFromNodeToBottom(Node n) {
        return (getCriticalPathWeightFromNodeToBottom(n) + .0d) / getCriticalPathWeightForGraph();
    }

    protected int getCriticalPathNumberFromNodeToBottom(Node n) {
        return findCriticalPathFromNodeToBottom(n).size();
    }

    protected int getCriticalPathNumberFromNodeToTop(Node n) {
        return findCriticalPathFromNodeToTop(n).size();
    }

    protected double getNormalCriticalPathNumberFromNodeToBottom(Node n) {
        return ((double) getCriticalPathNumberFromNodeToBottom(n)) / getLongestPathNumberForGraph();
    }

    /**
     * Returns longest path size.
     *
     * @return
     */
    protected int getLongestPathNumberForGraph() {
        return getLongestPathForGraph().size();
    }

    protected int getLongestPathNumberFromNodeToBottom(Node n) {
        return findLongestPathFromNodeToBottom(n).size();
    }

    protected List<Node> findCriticalPathFromNodeToTop(Node n) {
        Collection<Node> inNodes = n.getInputNodes();
        int criticalWeight = 0;
        List<Node> criticalPath = new ArrayList<>();

        for (Node current : inNodes) {
            List<Node> currentPath = findCriticalPathFromNodeToTop(current);
            final int currentWeight = calculatePathWeight(currentPath);
            if (currentWeight > criticalWeight) {
                criticalPath = currentPath;
                criticalWeight = currentWeight;
            }
        }

        criticalPath.add(0, n);
        return criticalPath;
    }

    protected List<Node> findCriticalPathFromNodeToBottom(Node n) {
        Collection<Node> outNodes = n.getOutputNodes();
        int criticalWeight = 0;
        List<Node> criticalPath = new ArrayList<>();

        for (Node current : outNodes) {
            List<Node> currentPath = findCriticalPathFromNodeToBottom(current);
            final int currentWeight = calculatePathWeight(currentPath);
            if (currentWeight > criticalWeight) {
                criticalPath = currentPath;
                criticalWeight = currentWeight;
            }
        }

        criticalPath.add(0, n);
        return criticalPath;
    }

    protected List<Node> findLongestPathFromNodeToTop(Node n) {
        Collection<Node> inNodes = n.getInputNodes();
        int numberOfNodes = 0;
        List<Node> criticalPath = new ArrayList<>();
        for (Node current : inNodes) {
            List<Node> currentPath = findLongestPathFromNodeToTop(current);
            final int currentNumberOfNodes = currentPath.size();
            if (currentNumberOfNodes > numberOfNodes) {
                criticalPath = currentPath;
                numberOfNodes = currentNumberOfNodes;
            }
        }

        criticalPath.add(0, n);
        return criticalPath;
    }

    protected List<Node> findLongestPathFromNodeToBottom(Node n) {
        Collection<Node> outNodes = n.getOutputNodes();
        int criticalWeight = 0;
        List<Node> criticalPath = new ArrayList<>();
        for (Node current : outNodes) {
            List<Node> currentPath = findLongestPathFromNodeToBottom(current);
            final int currentWeight = currentPath.size();
            if (currentWeight > criticalWeight) {
                criticalPath = currentPath;
                criticalWeight = currentWeight;
            }
        }
        criticalPath.add(0, n);
        return criticalPath;
    }

    /**
     * Returns the sum of nodes' weights, that are contained in the path list.
     *
     * @param path
     * @return
     */
    private int calculatePathWeight(List<Node> path) {
        int weight = 0;
        for (Node node : path) {
            weight += node.getWeight();
        }
        return weight;
    }

    public void printPath(List<Node> path, int criticalWeight) {
        for (Node n : path) {
            System.out.print(n.getId() + "@" + n.getWeight() + "  ");
        }
        System.out.println("/ " + criticalWeight);
    }
}
