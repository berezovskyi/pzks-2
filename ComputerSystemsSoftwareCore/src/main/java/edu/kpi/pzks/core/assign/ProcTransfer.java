package edu.kpi.pzks.core.assign;

import edu.kpi.pzks.core.model.Link;

/**
 *
 * @author Aloren
 */
public class ProcTransfer extends AbstractExecutable implements Executable {

    private final Link dataLink;
    private Processor toProcessor;
    private Link processorLink;
    private int channel;
    private boolean duplex;

    public ProcTransfer(Link link) {
        this.dataLink = link;
        this.executionTime = link.getWeight();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(dataLink.getFromNode().getId());
        sb.append("-->");
        sb.append(dataLink.getToNode().getId());
        sb.append("(").append(dataLink.getWeight()).append(")");
        sb.append("[").append(toProcessor.getId()).append("]");
        return sb.toString();
    }

    public Processor getToProcessor() {
        return toProcessor;
    }

    public void setToProcessor(Processor toProcessor) {
        this.toProcessor = toProcessor;
    }

    public Link getLink() {
        return dataLink;
    }

    public int getChannel() {
        return channel;
    }

    public void setChannel(int channel) {
        this.channel = channel;
    }

    public boolean isDuplex() {
        return duplex;
    }

    public void setDuplex(boolean duplex) {
        this.duplex = duplex;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ProcTransfer transfer = (ProcTransfer) o;

        if (toProcessor != null && !toProcessor.equals(transfer.toProcessor)) {
            return false;
        }
        if (processor != null && !processor.equals(transfer.processor)) {
            return false;
        }
        if (dataLink != null && !dataLink.equals(transfer.dataLink)) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = dataLink != null ? dataLink.hashCode() : 0;
        result = 31 * result + (toProcessor != null ? toProcessor.hashCode() : 0);
        result = 31 * result + (processor != null ? processor.hashCode() : 0);
        result = 31 * result + (dataLink != null ? dataLink.hashCode() : 0);
        return result;
    }

    public void setProcessorLink(Link betweenProcLink) {
        this.processorLink = betweenProcLink;
    }

    public Link getProcessorLink() {
        return processorLink;
    }
    
}
