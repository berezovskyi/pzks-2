package edu.kpi.pzks.core.queue.factors.impl;

import edu.kpi.pzks.core.model.Node;
import edu.kpi.pzks.core.queue.factors.FactorEvaluator;

/**
 * Variant 12
 *
 * У порядку спадання кількості вихідних дуг вершин.
 *
 * @author Aloren
 */
public class OutLinksFactorEvaluatorImpl implements FactorEvaluator {

    @Override
    public double evaluateFactorForNode(Node node) {
        return node.getOutputNodes().size();
    }

    @Override
    public boolean hasNaturalOrder() {
        return false;
    }
}
