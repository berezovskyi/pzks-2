package edu.kpi.pzks.core.assign;

import java.util.LinkedList;
import java.util.List;

/**
 * Author: snoop Date: 13.04.13 Time: 16:19
 */
public abstract class AbstractExecutable implements Executable {

    protected Processor processor;
    protected int startTime;
    protected int endTime;
    protected int executionTime;
    protected List<Executable> dependsOnList = new LinkedList<>();

    @Override
    public void exec(int reduceExecutionTimeBy) {
        executionTime -= reduceExecutionTimeBy;
    }

    @Override
    public int getExecLeft() {
        return executionTime;
    }

    @Override
    public int getStartTime() {
        return startTime;
    }

    @Override
    public int getEndTime() {
        return endTime;
    }

    @Override
    public int getExecutionTime() {
        return endTime - startTime;
    }

    @Override
    public void setStartTime(int time) {
        this.startTime = time;
    }

    @Override
    public void setEndTime(int time) {
        this.endTime = time;
    }

    @Override
    public Processor getProcessor() {
        return processor;
    }

    @Override
    public void setProcessor(Processor processor) {
        this.processor = processor;
    }

    @Override
    public List<Executable> getDependsOnList() {
        return dependsOnList;
    }

    @Override
    public void addDependency(Executable exec) {
        this.dependsOnList.add(exec);
    }

    @Override
    public void setState(ExecState state) {
        this.startTime = state.getStartTime();
        this.endTime = state.getEndTime();
        this.executionTime = state.getExec();
    }

    @Override
    public ExecState getState() {
        ExecState state = new ExecState();
        state.setStartTime(startTime);
        state.setEndTime(endTime);
        state.setExec(executionTime);
        return state;
    }
}
