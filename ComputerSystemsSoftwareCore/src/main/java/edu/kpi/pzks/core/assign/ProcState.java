package edu.kpi.pzks.core.assign;

/**
 * Author: snoop Date: 08.05.13 Time: 22:56
 */
public class ProcState {
    private int freeChannels;
    private boolean isFree;

    public int getFreeChannels() {
        return freeChannels;
    }

    public void setFreeChannels(int freeChannels) {
        this.freeChannels = freeChannels;
    }

    public boolean isFree() {
        return isFree;
    }

    public void setFree(boolean free) {
        isFree = free;
    }
}
