package edu.kpi.pzks.core.assign.impl;

import edu.kpi.pzks.core.assign.*;
import edu.kpi.pzks.core.model.Link;

import java.util.*;

/**
 * Author: snoop Date: 13.04.13 Time: 14:44
 */
public class Scheduler4Impl extends SchedulerImpl {


    @Override
    protected void schedule(Executable exec) {

        List<Processor> freeProcessors = new ArrayList<>();
        for (Processor processor : processors) {
            if (processor.isFree()) {
                freeProcessors.add(processor);
            }
        }

        Collections.sort(freeProcessors, getRemotenessComparator(exec));

        if (freeProcessors.isEmpty()) {
            return;
        }

        Processor winner = freeProcessors.get(0);

        assignTaskOnProcessor(exec, winner);
    }


    private Comparator<Processor> getRemotenessComparator(final Executable task) {
        return new Comparator<Processor>() {
            @Override
            public int compare(Processor p1, Processor p2) {
                int p1Time = 0, p2Time = 0;
                for (Executable parent : task.getDependsOnList()) {
                    int sendTime = 0;
                    if (parent instanceof ProcTransfer) {
                        sendTime = ((ProcTransfer) parent).getLink().getWeight();
                    } else if (parent instanceof ProcTask) {
                        for (Link l : tasks.getLinks()) {
                            if (l.getFromNode().equals(((ProcTask) parent).getQueuedNode().getNode()) &&
                                    l.getToNode().equals(((ProcTask) task).getQueuedNode().getNode())) {
                                sendTime = l.getWeight();
                                break;
                            }
                        }
                    }
                    if (!parent.getProcessor().equals(p1)) {
                        p1Time += p1.getRoutingTable().get(parent.getProcessor()).size() * sendTime;
                    }
                    if (!parent.getProcessor().equals(p2)) {
                        p2Time += p2.getRoutingTable().get(parent.getProcessor()).size() * sendTime;
                    }
                }

                if (p1Time < p2Time) {
                    return -1;
                } else if (p1Time > p2Time) {
                    return 1;
                } else {
                    return 0;
                }
            }
        };
    }

}
