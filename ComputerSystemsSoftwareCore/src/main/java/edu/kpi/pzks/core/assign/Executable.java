package edu.kpi.pzks.core.assign;

import java.util.List;

/**
 *
 * @author Aloren
 */
public interface Executable {

    /**
     *   execute part of the executable
     *   @param part execute some part of the executable depending on proc performance/channel bandwidth
     */

    void exec(int part);

    /**
     * get executable units left for execution of the current executable
     * @return executable units
     */
    int getExecLeft();

    /**
     * Get start time of this executable
     * @return the start time
     */
    int getStartTime();

    /**
     * Get end time of this executable
     * @return the end time
     */
    int getEndTime();

    /**
     * Get total execution time of this executable
     * @return the execution time
     */
    int getExecutionTime();

    /**
     * Set the start time. Called during simulation.
     * @param time  the start time of this executable. ({@link #getStartTime()}
     */
    void setStartTime(int time);

    /**
     * Set the end time. Called during simulation.
     * @param time  the end time of this executable. ({@link #getEndTime()}
     */
    void setEndTime(int time);

    /**
     * Set the processor, on which this executable is executed. Called during planning by scheduler.
     * @param processor  the processor. {@link #getProcessor()}
     */
    void setProcessor(Processor processor);

    /**
     * @return  {@link #setProcessor(Processor)}
     */
    Processor getProcessor();

    /**
     * Filled during planning phase. Used by simulator to determine ready for execution executables.
     * @return List of upper-dependent ancestors of this executable.
     */
    List<Executable> getDependsOnList();

    /**
     * Adds executable which this executable depends on. {@link #getDependsOnList()}
     * @param exec upper-level executable.
     */
    void addDependency(Executable exec);

    void setState(ExecState state);
    ExecState getState();
}
