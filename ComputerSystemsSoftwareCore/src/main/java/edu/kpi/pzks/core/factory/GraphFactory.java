package edu.kpi.pzks.core.factory;

import edu.kpi.pzks.core.model.Graph;
import edu.kpi.pzks.core.model.Link;
import edu.kpi.pzks.core.model.Links;
import edu.kpi.pzks.core.model.Node;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 *
 * @author Aloren
 */
public class GraphFactory {

    private static int nodesWeightSum;
    private static double remainingLinkWeight;
    private static int minLinkWeight;
    private final static int MAX_NUMBER_NODES = 400;

    public static Graph newGraph(int numberOfNodes, int minNodeWeight, int maxNodeWeight, double connectivity) {
        checkInputData(connectivity, numberOfNodes, minNodeWeight, maxNodeWeight);

        Graph graph = new Graph(true);
        Random generator = new Random();
        nodesWeightSum = 0;
        List<Node> nodes = generateNodes(numberOfNodes, maxNodeWeight, minNodeWeight);
        double linksWeightSum = nodesWeightSum / connectivity - nodesWeightSum;
        remainingLinkWeight = linksWeightSum;
        Node[] nodesArr = nodes.toArray(new Node[nodes.size()]);
        Links links = new Links();
        final double shouldCreateLink = 0.3;
        final double shouldIncWeightLink = 1;
        final double retainLinkWeight = 0.1; // with patience comes precision
        minLinkWeight = 1;
        int sum = 0;
//        System.out.println("LinksWeightSum: " + linksWeightSum);
        while (!finished()) {
            int i = 0;
            while (i < nodesArr.length && !finished()) {
                int j = i + 1;
                while (j < nodesArr.length && !finished()) {
                    if (generator.nextDouble() <= shouldCreateLink) {
                        final Node fromNode = nodesArr[i];
                        final Node toNode = nodesArr[j];
                        final Link linkBetween = links.getLinkBetween(fromNode, toNode);
                        final int maxWeight = (int) Math.ceil(remainingLinkWeight * retainLinkWeight);
                        if (linkBetween == null) {
                            int generatedWeight = generateWeight(minLinkWeight, maxWeight);
                            remainingLinkWeight -= generatedWeight;
                            Link link = new Link(fromNode, toNode, generatedWeight);
                            links.add(link);
                            sum += generatedWeight;
//                            System.out.println("Gen link: " + link.getWeight());
                        } else {
                            if (generator.nextDouble() <= shouldIncWeightLink) {
                                int generatedWeight = generateWeight(minLinkWeight, maxWeight);
                                remainingLinkWeight -= generatedWeight;
                                final int oldWeight = linkBetween.getWeight();
                                linkBetween.setWeight(oldWeight + generatedWeight);
                                sum += generatedWeight;
//                                System.out.println("Inc weight: " + oldWeight + " new weight: " + linkBetween.getWeight());
                            }
                        }
                    }
                    j++;
                }
                i++;
            }
        }
//        System.out.println("Sum: " + sum);
        graph.addNodes(nodes);
        graph.addLinks(links);
        return graph;
    }

    private static boolean finished() {
        return remainingLinkWeight < minLinkWeight;
    }

    protected static List<Node> generateNodes(int numberOfNodes, int maxNodeWeight, int minNodeWeight) {
        List<Node> nodes = new ArrayList<>();
        for (int i = 0; i < numberOfNodes; i++) {
            int weight = generateWeight(minNodeWeight, maxNodeWeight);
            nodesWeightSum += weight;
            Node node = new Node(weight);
            node.setId(i);
            nodes.add(node);
        }
        return nodes;
    }

    private static int generateWeight(int minNodeWeight, int maxNodeWeight) {
        if (minNodeWeight > maxNodeWeight) {
            throw new IllegalArgumentException("Cannot generate random in current limits: [" + minNodeWeight + "; " + maxNodeWeight + "]");
        }
        if (minNodeWeight == maxNodeWeight) {
            return minNodeWeight;
        }
        int weight = minNodeWeight + (int) (Math.random() * ((maxNodeWeight - minNodeWeight) + 1));
        return weight;
    }

    private static void checkInputData(double connectivity, int numberOfNodes,
            int minNodeWeight, int maxNodeWeight) throws IllegalArgumentException {
        if (connectivity > 1 || connectivity < 0.000001) {
            throw new IllegalArgumentException("Connectivity = " + connectivity + ". connectivity <=1 and >0");
        }
        if (numberOfNodes <= 0 || numberOfNodes > MAX_NUMBER_NODES) {
            throw new IllegalArgumentException("Illegal number of nodes: " + numberOfNodes + ". Must be not greater than " + MAX_NUMBER_NODES);
        }
        if (minNodeWeight > maxNodeWeight) {
            throw new IllegalArgumentException("Min node weight can not be greater or eqault to max node weight!");
        }
        if (minNodeWeight < 0 || maxNodeWeight < 0) {
            throw new IllegalArgumentException("Max or min node weights can not b less than 0.");
        }
    }
}
