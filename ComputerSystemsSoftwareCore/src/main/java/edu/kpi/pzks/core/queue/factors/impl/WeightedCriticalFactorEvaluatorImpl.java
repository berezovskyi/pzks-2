package edu.kpi.pzks.core.queue.factors.impl;

import edu.kpi.pzks.core.model.Link;
import edu.kpi.pzks.core.model.Node;
import edu.kpi.pzks.core.queue.factors.AbstractFactorEvaluator;

import java.util.Collection;

/**
 * Метод формирования очередей №1
 *
 * У порядку спадання пронормованої суми 
 * критичних по часу і по кількості вершин 
 * шляхів до кінця графа задачі.
 *
 * @author smarx
 */
public class WeightedCriticalFactorEvaluatorImpl extends AbstractFactorEvaluator {

    public WeightedCriticalFactorEvaluatorImpl(Collection<Node> nodes, Collection<Link> links) {
        super(nodes, links);
    }

    @Override
    public double evaluateFactorForNode(Node node) {
        double path = getNormalCriticalPathWeightFromNodeToBottom(node);
        double number = getNormalCriticalPathNumberFromNodeToBottom(node);
//        System.out.printf("For node %s: path=%f; number=%f\n", node, path, number);
        return path + number; // пронормированная сумма
    }

    @Override
    public boolean hasNaturalOrder() {
        return false; // в порядке убывания
    }
}
