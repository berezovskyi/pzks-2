package edu.kpi.pzks.core.queue.factors.impl;

import edu.kpi.pzks.core.model.Link;
import edu.kpi.pzks.core.model.Node;
import edu.kpi.pzks.core.queue.factors.AbstractFactorEvaluator;

import java.util.Collection;

/**
 * Task #4; part1
 *
 * @author smarx
 */
public class CriticalNumberDescFactorEvaluatorImpl extends AbstractFactorEvaluator {

    public CriticalNumberDescFactorEvaluatorImpl(Collection<Node> nodes, Collection<Link> links) {
        super(nodes, links);
    }

    @Override
    public double evaluateFactorForNode(Node node) {
        return getCriticalPathNumberFromNodeToBottom(node);
    }

    @Override
    public boolean hasNaturalOrder() {
        return false;
    }
}
