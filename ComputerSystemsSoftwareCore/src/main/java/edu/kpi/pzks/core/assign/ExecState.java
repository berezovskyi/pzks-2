package edu.kpi.pzks.core.assign;

/**
 * Author: snoop Date: 06.05.13 Time: 14:10
 */
public class ExecState {
    private int startTime;
    private int endTime;
    private int exec;

    public int getStartTime() {
        return startTime;
    }

    public void setStartTime(int startTime) {
        this.startTime = startTime;
    }

    public int getEndTime() {
        return endTime;
    }

    public void setEndTime(int endTime) {
        this.endTime = endTime;
    }

    public int getExec() {
        return exec;
    }

    public void setExec(int exec) {
        this.exec = exec;
    }
}
