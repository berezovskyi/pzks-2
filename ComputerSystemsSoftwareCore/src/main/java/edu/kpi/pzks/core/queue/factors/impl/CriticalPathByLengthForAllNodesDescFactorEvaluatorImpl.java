package edu.kpi.pzks.core.queue.factors.impl;

import edu.kpi.pzks.core.model.Link;
import edu.kpi.pzks.core.model.Node;
import edu.kpi.pzks.core.queue.factors.AbstractFactorEvaluator;

import java.util.Collection;

/**
 * Метод формирования очередей №6
 *
 * @author Snoop
 */
public class CriticalPathByLengthForAllNodesDescFactorEvaluatorImpl extends AbstractFactorEvaluator {
    public CriticalPathByLengthForAllNodesDescFactorEvaluatorImpl(Collection<Node> nodes, Collection<Link> links) {
        super(nodes, links);
    }

    @Override
    public double evaluateFactorForNode(Node node) {
        return getLongestPathNumberFromNodeToBottom(node);
    }

    @Override
    public boolean hasNaturalOrder() {
        return false; // в порядке убывания
    }
}
