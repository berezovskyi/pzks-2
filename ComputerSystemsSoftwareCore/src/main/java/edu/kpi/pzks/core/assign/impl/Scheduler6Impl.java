package edu.kpi.pzks.core.assign.impl;

import edu.kpi.pzks.core.assign.*;
import edu.kpi.pzks.core.model.Link;
import edu.kpi.pzks.core.model.Node;

import java.util.*;

/**
 * Author: snoop Date: 13.04.13 Time: 14:44
 */
public class Scheduler6Impl extends SchedulerImpl {

    protected Map<Executable, ExecState> execBackup = new HashMap<>();
    protected Map<Processor, ProcState> procBackup = new HashMap<>();
    protected Map<Processor, Integer> tempMinTransferTime = new HashMap<>();

    /**
     * Schedule initial tasks on the processors
     */
    @Override
    public void schedule(SimulationInfo simulationInfo) {
        super.schedule(simulationInfo);
        for (Executable exec : events) {
            execBackup.put(exec, exec.getState());
        }
    }

    @Override
    protected void schedule(Executable exec) {
        tempMinTransferTime.clear();

        List<Processor> freeProcessors = new ArrayList<>();
        for (Processor processor : processors) {
            if (processor.isFree()) {
                freeProcessors.add(processor);
            }
        }

        if (freeProcessors.isEmpty()) {
            return;
        }


        Collections.sort(freeProcessors, getComparator(exec));

        Processor winner = freeProcessors.get(0);
        if (freeProcessors.size() > 1) {
            backupStates();
            int oldEstimation = 99999999;
            for (int i = 0; i < freeProcessors.size() - 1; i++) {
                List<Executable> tempTransfers = assignTaskOnProcessor(exec, freeProcessors.get(i));
                int expectedNextMinTime = tempMinTransferTime.get(freeProcessors.get(i + 1));
                int estimatedTime = estimateTime(exec);
                events.removeAll(tempTransfers);
                exec.getDependsOnList().removeAll(tempTransfers);
                restoreStates();

                if (estimatedTime > oldEstimation) {
                    winner = freeProcessors.get(i - 1);
                }
                oldEstimation = estimatedTime;
                if (estimatedTime <= expectedNextMinTime) {
                    winner = freeProcessors.get(i);
                    break;
                }
            }
        }

        assignTaskOnProcessor(exec, winner);
        for (Executable e : events) {
            execBackup.put(e, e.getState());
        }
    }

    private void backupStates() {
        for (Processor processor : processors) {
            procBackup.put(processor, processor.getState());
        }
        for (Executable e : execBackup.keySet()) {
            execBackup.put(e, e.getState());
        }
    }

    private void restoreStates() {
        for (Executable e : execBackup.keySet()) {
            e.setState(execBackup.get(e));
        }
        for (Processor processor : processors) {
            processor.setState(procBackup.get(processor));
        }
    }

    private Comparator<Processor> getComparator(final Executable task) {
        return new Comparator<Processor>() {
            @Override
            public int compare(Processor p1, Processor p2) {
                int p1Time = 0, p2Time = 0;
                for (Executable dependency : task.getDependsOnList()) {
                    int sendTime = 0;
                    if (dependency instanceof ProcTransfer) {
                        sendTime = ((ProcTransfer) dependency).getLink().getWeight();
                    } else if (dependency instanceof ProcTask) {
                        for (Link link : tasks.getLinks()) {
                            final Node linkSrc = link.getFromNode();
                            final Node linkDst = link.getToNode();
                            final Node dependencyNode = ((ProcTask) dependency).getQueuedNode().getNode();
                            final Node taskNode = ((ProcTask) task).getQueuedNode().getNode();
                            if (linkSrc.equals(dependencyNode) && linkDst.equals(taskNode)) {
                                sendTime = link.getWeight();
                                break;
                            }
                        }
                    }
                    final Processor dependencyProcessor = dependency.getProcessor();
                    if (!dependencyProcessor.equals(p1)) {
                        p1Time += p1.getRoutingTable().get(dependencyProcessor).size() * sendTime;
                    }
                    if (!dependencyProcessor.equals(p2)) {
                        p2Time += p2.getRoutingTable().get(dependencyProcessor).size() * sendTime;
                    }
                }

                tempMinTransferTime.put(p1, p1Time);
                tempMinTransferTime.put(p2, p2Time);

                if (p1Time < p2Time) {
                    return -1;
                } else if (p1Time > p2Time) {
                    return 1;
                } else {
                    return 0;
                }
            }
        };
    }

    private int estimateTime(Executable task) {
        int time = 0; //time unit - 1 processor clock
        List<Executable> waitingEvents = new LinkedList<>(events);
        List<Executable> completedEvents = new LinkedList<>();
        List<Executable> runningEvents = new LinkedList<>();
        List<ProcTransfer> runningTransfers = new LinkedList<>();

        for (Executable exec : execBackup.keySet()) {
            if (exec.getEndTime() != 0 && exec.getExecLeft() <= 0) {
                completedEvents.add(exec);
            }
            if ((exec.getStartTime() != 0 || exec.getDependsOnList().isEmpty())
                    && exec.getEndTime() == 0 && exec.getExecLeft() > 0 && exec.getProcessor() != null) {
                runningEvents.add(exec);
                if (exec instanceof ProcTransfer) {
                    runningTransfers.add((ProcTransfer) exec);
                }
            }
        }

        boolean started = false;
        while (!started) {
            //run something
            for (Executable runningEvent : runningEvents) {
                if (runningEvent instanceof ProcTask) {
                    final Processor processor = runningEvent.getProcessor();
                    final int performance = processor.getPerformance();
//                    System.out.println("Executing " + event + " on " + processor + " performance: " + performance);
                    runningEvent.exec(performance);
                } else if (runningEvent instanceof ProcTransfer) {
                    ProcTransfer transfer = (ProcTransfer) runningEvent;
                    final int bandwidth = transfer.getProcessorLink().getWeight();
//                    System.out.println("Executing " + event + " on " + transfer.getLink() + " bandwidth: " + bandwidth);
                    runningEvent.exec(bandwidth);
                }
                //check if completed, possibly <0 (as we subtract >0 integer each time)
                if (runningEvent.getExecLeft() <= 0) {
                    runningEvent.setEndTime(time);
                    completedEvents.add(runningEvent);
                    if (runningEvent instanceof ProcTransfer) {
                        ProcTransfer transfer = (ProcTransfer) runningEvent;
                        if (!transfer.isDuplex()) {
                            transfer.getProcessor().releaseChannel();
                            transfer.getToProcessor().releaseChannel();
                        }
                        runningTransfers.remove(runningEvent);
                    } else {
                        runningEvent.getProcessor().setFree(true);
                    }
                }
            }
            runningEvents.removeAll(completedEvents);

            //look through waiting list if we can start running anything
            for (Executable waitingEvent : waitingEvents) {
                boolean canStart = true;
                for (Executable dependency : waitingEvent.getDependsOnList()) {
                    if (!completedEvents.contains(dependency)) {
                        canStart = false;
                    }
                }
                if (canStart) {
                    if (waitingEvent instanceof ProcTask) {
                        if (waitingEvent.equals(task)) {
                            started = true;
                        }
                        if (waitingEvent.getProcessor() != null) {
                            waitingEvent.getProcessor().setFree(false);
                            waitingEvent.setStartTime(time);
                            runningEvents.add(waitingEvent);
                        }
                    } else {
                        ProcTransfer transfer = (ProcTransfer) waitingEvent;
                        //check if both processors have free channel
                        boolean hasFreeChannels =
                                transfer.getProcessor().hasFreeChannels() && transfer.getToProcessor()
                                .hasFreeChannels();
                        boolean isFree = true;
                        for (ProcTransfer conflict : runningTransfers) {
                            //check whether this link is occupied
                            if (conflict.getToProcessor().equals(transfer.getToProcessor()) && conflict.getProcessor().equals(transfer.getProcessor())) {
                                isFree = false;
                                break;
                            }
                            //check if someone is sending info in the opposite way (only for half duplex)
                            if (simulationInfo.getLinkMode() == SimulationInfo.LinkMode.HALF_DUPLEX) {
                                if (conflict.getToProcessor().equals(transfer.getProcessor()) && conflict
                                        .getProcessor().equals(transfer.getToProcessor())) {
                                    isFree = false;
                                    break;
                                }
                            } //if duplex can work with one channel - TODO ask Rusanova
                            else {
                                if (conflict.getToProcessor().equals(transfer.getProcessor()) && conflict
                                        .getProcessor().equals(transfer.getToProcessor())) {
                                    transfer.setChannel(conflict.getChannel());
                                    transfer.setDuplex(true);
                                    break;
                                }
                            }
                        }
                        if (isFree && hasFreeChannels || transfer.isDuplex()) {

                            if (!transfer.isDuplex() && hasFreeChannels) {
                                int channel = transfer.getProcessor().occupyChannel();
                                transfer.getToProcessor().occupyChannel();
                                transfer.setChannel(channel);
                            }
                            transfer.setStartTime(time);
                            runningEvents.add(transfer);
                            runningTransfers.add(transfer);
                        }
                    }
                }
            }
            waitingEvents.removeAll(runningEvents);
            time++;
        }
        return time - 1;
    }

    @Override
    public String toString() {
        return "Алгоритм №6";
    }
}
