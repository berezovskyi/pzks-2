package edu.kpi.pzks.core.queue;

import edu.kpi.pzks.core.queue.utils.DoubleFactorNodeComparator;
import edu.kpi.pzks.core.queue.factors.FactorEvaluator;
import edu.kpi.pzks.core.model.Node;

import java.util.*;

/**
 * @author smarx
 */
public class DoubleFactorQueue implements Queue {
    private Collection<Node> nodes;
    private final FactorEvaluator primaryFactor;
    private final FactorEvaluator secondaryFactor;

    public DoubleFactorQueue(FactorEvaluator primaryFactor, FactorEvaluator secondaryFactor, Collection<Node> nodes) {
        this.primaryFactor = primaryFactor;
        this.secondaryFactor = secondaryFactor;
        this.nodes = nodes;
    }

    @Override
    public Collection<QueuedNode> evaluate() {
        DoubleFactorNodeComparator comparator = new DoubleFactorNodeComparator(primaryFactor, secondaryFactor);

        List<Node> nodeList = new ArrayList<>(nodes);
        Collections.sort(nodeList, comparator);

        List<QueuedNode> queuedNodes = new ArrayList<>();
        for(Node n : nodeList) {
            QueuedNode queuedNode = new DoubleQueuedNode(n, primaryFactor.evaluateFactorForNode(n), secondaryFactor.evaluateFactorForNode(n));
            queuedNodes.add(queuedNode);
        }

        return queuedNodes;
    }

}
