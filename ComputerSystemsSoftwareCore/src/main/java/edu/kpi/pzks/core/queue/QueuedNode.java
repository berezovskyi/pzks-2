package edu.kpi.pzks.core.queue;

import edu.kpi.pzks.core.model.Node;

/**
 * @author smarx
 */
public class QueuedNode {

    protected Node node;
    protected double queueingFactor;

    public QueuedNode(Node node, double queueingFactor) {
        this.node = node;
        this.queueingFactor = queueingFactor;
    }

    public Node getNode() {
        return node;
    }

    public double getQueueingFactor() {
        return queueingFactor;
    }

    @Override
    public String toString() {
        return String.format("%d (%.4f)", node.getId(), queueingFactor);
    }
}
