package edu.kpi.pzks.core.assign.impl;

import edu.kpi.pzks.core.assign.Executable;
import edu.kpi.pzks.core.assign.Processor;

/**
 * Author: snoop Date: 13.04.13 Time: 14:44
 */
public class Scheduler3Impl extends SchedulerImpl {

    protected void schedule(Executable exec) {
        Processor candidate = null;
        for(Processor processor: processors) {
            if(processor.isFree()) {
                candidate = processor;
                break;
            }
        }
        if(candidate == null) {
            return;
        }
        assignTaskOnProcessor(exec, candidate);
    }

    @Override
    public String toString() {
        return "Алгоритм №3";
    }
}
