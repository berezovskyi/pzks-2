package edu.kpi.pzks.core.queue.utils;

import edu.kpi.pzks.core.queue.factors.FactorEvaluator;
import edu.kpi.pzks.core.model.Node;

import java.util.Comparator;

/**
 * @author smarx
 */
public class DoubleFactorNodeComparator implements Comparator<Node> {

    private final SingleFactorNodeComparator primaryNodeComparator;
    private final SingleFactorNodeComparator secondaryNodeComparator;

    public DoubleFactorNodeComparator(FactorEvaluator primaryFactor, FactorEvaluator secondaryFactor) {
        this.primaryNodeComparator = new SingleFactorNodeComparator(primaryFactor);
        this.secondaryNodeComparator = new SingleFactorNodeComparator(secondaryFactor);
    }

    @Override
    public int compare(Node n1, Node n2) {
        int primaryComparison = primaryNodeComparator.compare(n1, n2);
        if (primaryComparison != 0) {
            return primaryComparison;
        }
        return secondaryNodeComparator.compare(n1, n2);
    }
}
