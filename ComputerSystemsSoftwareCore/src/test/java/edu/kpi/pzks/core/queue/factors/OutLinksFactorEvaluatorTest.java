package edu.kpi.pzks.core.queue.factors;

import edu.kpi.pzks.core.queue.factors.FactorEvaluator;
import edu.kpi.pzks.core.model.Node;
import edu.kpi.pzks.core.queue.Queue;
import edu.kpi.pzks.core.queue.QueuedNode;
import edu.kpi.pzks.core.queue.SingleFactorQueue;
import edu.kpi.pzks.core.queue.factors.impl.OutLinksFactorEvaluatorImpl;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.Set;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 *
 * @author Aloren
 */
public class OutLinksFactorEvaluatorTest {

    private static final int NUM_NODES = 10;

    @Test
    public void test() {
        FactorEvaluator factorEvaluator = new OutLinksFactorEvaluatorImpl();
        List<Integer> outputLinks = generateNumOutputLinks(NUM_NODES);
        Collection<Node> nodes = generateNodes(NUM_NODES, outputLinks);

        assertTrue("Number of output links must be equal to the number of nodes", nodes.size() == outputLinks.size());

        Queue q = new SingleFactorQueue(factorEvaluator, nodes);
        Collection<QueuedNode> queuedNodes = q.evaluate();

        assertTrue(queuedNodes.size() == nodes.size());

        Collections.sort(outputLinks, Collections.reverseOrder());

        Iterator<Integer> linksIterator = outputLinks.iterator();
        Iterator<QueuedNode> queueIterator = queuedNodes.iterator();
        while (linksIterator.hasNext()) {
            QueuedNode queuedNode = queueIterator.next();
            Integer numLinks = linksIterator.next();
            assertTrue("Factors(number of output links) must be sorted in the descending order", queuedNode.getQueueingFactor() == numLinks);
        }
    }

    private Collection<Node> generateNodes(int NUM_NODES, List<Integer> outputLinks) {
        Collection<Node> nodes = new LinkedList<>();
        for (int i = 0; i < NUM_NODES; i++) {
            Node node = mock(Node.class);

            final Integer numOutLinks = outputLinks.get(i);
            when(node.getOutputNodes()).thenReturn(generateDummyNodes(numOutLinks));
            nodes.add(node);
        }
        return nodes;
    }

    private List<Integer> generateNumOutputLinks(int NUM_NODES) {
        List<Integer> outputLinks = new LinkedList<>();
        Random random = new Random();
        for (int i = 0; i < NUM_NODES; i++) {
            int numLinks = random.nextInt(100);
            outputLinks.add(numLinks);
        }
        return outputLinks;
    }

    private Set<Node> generateDummyNodes(Integer numOutLinks) {
        Set<Node> nodes = new HashSet<>();
        for (int i = 0; i < numOutLinks; i++) {
            nodes.add(mock(Node.class));
        }
        return nodes;
    }
}
