package edu.kpi.pzks.core.queue.factors;

import edu.kpi.pzks.core.queue.factors.AbstractFactorEvaluator;
import edu.kpi.pzks.core.model.Link;
import edu.kpi.pzks.core.model.Node;
import edu.kpi.pzks.core.queue.factors.impl.ConnectivityDescFactorEvaluatorImpl;
import edu.kpi.pzks.core.queue.factors.impl.WeightedCriticalFactorEvaluatorImpl;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Mockito.mock;

/**
 * @author smarx
 */
public class AbstractFactorEvaluatorTest {

    private AbstractFactorEvaluator fe;

    @Test
    public void testLinearWeight() {
        Node nodeA = new Node(1, 0);
        Node nodeB = new Node(7, 1);
        Node nodeC = new Node(3, 2);
        Collection<Node> nodes = Arrays.asList(new Node[]{nodeA, nodeB, nodeC});
        Collection<Link> links = Arrays.asList(new Link[]{new Link(nodeA, nodeB), new Link(nodeB, nodeC)});
        fe = new ConnectivityDescFactorEvaluatorImpl(nodes, links);

        assertEquals(11, fe.getCriticalPathWeightForGraph());
        assertEquals(11, fe.getCriticalPathWeightFromNodeToBottom(nodeA));
        assertEquals(10, fe.getCriticalPathWeightFromNodeToBottom(nodeB));
        assertEquals(3, fe.getCriticalPathWeightFromNodeToBottom(nodeC));
    }

    @Test
    public void testBottomCut() {
        Node nodeA = new Node(1, 0);
        Node nodeB = new Node(7, 1);
        Node nodeC = new Node(3, 2);
        Node nodeD = new Node(5, 3);
        List<Node> nodes = Arrays.asList(nodeA, nodeB, nodeC, nodeD);
        Collection<Link> links = Arrays.asList(new Link(nodeA, nodeB),
                new Link(nodeB, nodeC),
                new Link(nodeB, nodeD));
        LoopCheckCriticalPath(nodes, links, 13);
    }

    @Test
    public void testTopAnBottomCut() {
        Node nodeA = new Node(1, 0);   //   1   7
        Node nodeA2 = new Node(7, 1);  //   \  x
        Node nodeB = new Node(7, 2);   //    7
        Node nodeC = new Node(3, 3);   //   /x
        Node nodeD = new Node(5, 4);   //  3  5 ====> 19
        List<Node> nodes = Arrays.asList(nodeA, nodeA2, nodeB, nodeC, nodeD);
        Collection<Link> links = Arrays.asList(new Link(nodeA, nodeB),
                new Link(nodeA2, nodeB),
                new Link(nodeB, nodeC),
                new Link(nodeB, nodeD));

        LoopCheckCriticalPath(nodes, links, 19);
    }

    /**
     * The graph reflected in code below is a courtesy of Rusanova et al. ;-)
     * Source: Lecture no. 1
     */
    @Test
    public void testTopAnBottomCutWithComponents() {
        Node nodeA = new Node(1, 0);   //   1   7     1
        Node nodeA2 = new Node(7, 1);  //   \  x      |
        Node nodeB = new Node(7, 2);   //    7        5
        Node nodeC = new Node(3, 3);   //   /x
        Node nodeD = new Node(5, 4);   //  3  5 ====> 19
        Node nodeE = new Node(1, 5);
        Node nodeF = new Node(5, 6);

        List<Node> nodes = Arrays.asList(nodeA, nodeA2, nodeB, nodeC, nodeD, nodeE, nodeF);
        Collection<Link> links = Arrays.asList(new Link(nodeA, nodeB),
                new Link(nodeA2, nodeB),
                new Link(nodeB, nodeC),
                new Link(nodeB, nodeD),
                new Link(nodeE, nodeF)
        );

        LoopCheckCriticalPath(nodes, links, 19);
    }

    @Test
    public void testTopAnBottomCutWithHeavyComponents() {
        Node nodeA = new Node(1, 0);   //   1   7     12
        Node nodeA2 = new Node(7, 1);  //   \  x      |
        Node nodeB = new Node(7, 2);   //    7        15 ===> 27
        Node nodeC = new Node(3, 3);   //   /x
        Node nodeD = new Node(5, 4);   //  3  5 ====> 19
        Node nodeE = new Node(12, 5);
        Node nodeF = new Node(15, 6);

        List<Node> nodes = Arrays.asList(nodeA, nodeA2, nodeB, nodeC, nodeD, nodeE, nodeF);
        Collection<Link> links = Arrays.asList(new Link(nodeA, nodeB),
                new Link(nodeA2, nodeB),
                new Link(nodeB, nodeC),
                new Link(nodeB, nodeD),
                new Link(nodeE, nodeF)
        );

        LoopCheckCriticalPath(nodes, links, 27);
    }

    @Test
    public void testWeightedCriticalToLeafOnPathEqualsOne() throws Exception {
        Node nodeA = new Node(1, 0);   //   1   7     1
        Node nodeA2 = new Node(7, 1);  //   \  x      |
        Node nodeB = new Node(7, 2);   //    7        5
        Node nodeC = new Node(3, 3);   //   /x
        Node nodeD = new Node(5, 4);   //  3  5 ====> 19
        Node nodeE = new Node(1, 5);
        Node nodeF = new Node(5, 6);

        List<Node> nodes = Arrays.asList(nodeA, nodeA2, nodeB, nodeC, nodeD, nodeE, nodeF);
        Collection<Link> links = Arrays.asList(new Link(nodeA, nodeB),
                new Link(nodeA2, nodeB),
                new Link(nodeB, nodeC),
                new Link(nodeB, nodeD),
                new Link(nodeE, nodeF)
        );

        fe = new ConnectivityDescFactorEvaluatorImpl(nodes, links);
        assertEquals(fe.getCriticalPathWeightForGraph(), fe.getCriticalPathWeightFromNodeToBottom(nodeA2));
        assertTrue("Weighted critical path of leaf node on critical path must be equal to 1",
                Double.compare(1.0, fe.getNormalCriticalPathWeightFromNodeToBottom(nodeA2)) == 0);
    }

    @Test
    public void testCriticalPathIsCorrect() throws Exception {
        Node node0 = new Node(1, 0);
        Node node1 = new Node(9, 1);
        Node node2 = new Node(1, 2);
        Node node3 = new Node(1, 3);
        Node node4 = new Node(1, 4);
        Node node5 = new Node(7, 5);

        List<Node> nodes = Arrays.asList(node0, node1, node2, node3, node4, node5);
        List<Link> links = Arrays.asList(new Link(node0, node3), new Link(node1, node2), new Link(node2, node4), new Link(node3, node4));

        fe = new WeightedCriticalFactorEvaluatorImpl(nodes, links);

        assertEquals(3, fe.getCriticalPathForGraph().size());
        assertEquals(11, fe.getCriticalPathWeightForGraph());
        assertEquals(3, fe.getLongestPathNumberForGraph());
    }

    @Test
    public void testCriticalPathWithHeavyWeightIsCorrect() throws Exception {
        Node node0 = new Node(1, 0);
        Node node1 = new Node(9, 1);
        Node node2 = new Node(1, 2);
        Node node3 = new Node(1, 3);
        Node node4 = new Node(1, 4);
        Node node5 = new Node(71, 5);

        List<Node> nodes = Arrays.asList(node0, node1, node2, node3, node4, node5);
        List<Link> links = Arrays.asList(new Link(node0, node3), new Link(node1, node2), new Link(node2, node4), new Link(node3, node4));

        fe = new WeightedCriticalFactorEvaluatorImpl(nodes, links);

        assertEquals(1, fe.getCriticalPathForGraph().size());
        assertEquals(71, fe.getCriticalPathWeightForGraph());
        assertEquals(3, fe.getLongestPathNumberForGraph());
    }

    @Test
    public void testCriticalNumber() throws Exception {
        Node node1 = new Node(2, 1);
        Node node2 = new Node(3, 2);
        Node node4 = new Node(3, 4);
        Node node6 = new Node(1, 6);
        Node node7 = new Node(1, 6);
        Node node8 = new Node(4, 8);

        Node node3 = new Node(1, 3);
        Node node5 = new Node(5, 5);

        List<Node> nodes = Arrays.asList(node1, node2, node4, node6, node7, node3, node5);
        Collection<Link> links = Arrays.asList(new Link(node1, node4),
                new Link(node2, node4),
                new Link(node4, node6),
                new Link(node6, node7),
                new Link(node6, node8),
                new Link(node3, node5)
        );

        fe = new ConnectivityDescFactorEvaluatorImpl(nodes, links);
        assertEquals(4, fe.getCriticalPathNumberFromNodeToBottom(node1));
        assertEquals(4, fe.getCriticalPathNumberFromNodeToBottom(node2));
        assertEquals(2, fe.getCriticalPathNumberFromNodeToBottom(node3));
        assertEquals(3, fe.getCriticalPathNumberFromNodeToBottom(node4));
        assertEquals(1, fe.getCriticalPathNumberFromNodeToBottom(node5));
        assertEquals(2, fe.getCriticalPathNumberFromNodeToBottom(node6));
        assertEquals(1, fe.getCriticalPathNumberFromNodeToBottom(node7));
        assertEquals(1, fe.getCriticalPathNumberFromNodeToBottom(node8));
    }

    private void LoopCheckCriticalPath(List<Node> nodes, Collection<Link> links,  int weight) {
        for (int i = 0; i < 10; i++) {
            Collections.shuffle(nodes);
            fe = new ConnectivityDescFactorEvaluatorImpl(nodes, links);
            assertEquals("Failed on i=" + i, weight, fe.getCriticalPathWeightForGraph());
        }
    }
}
