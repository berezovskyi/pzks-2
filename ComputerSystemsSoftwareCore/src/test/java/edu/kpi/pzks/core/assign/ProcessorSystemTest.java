package edu.kpi.pzks.core.assign;

import edu.kpi.pzks.core.assign.impl.Scheduler3Impl;
import edu.kpi.pzks.core.model.Graph;
import edu.kpi.pzks.core.model.Link;
import edu.kpi.pzks.core.model.Links;
import edu.kpi.pzks.core.model.Node;
import edu.kpi.pzks.core.queue.SingleFactorQueue;
import edu.kpi.pzks.core.queue.factors.impl.NodeWeightAscFactorEvaluatorImpl;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Author: snoop Date: 13.04.13 Time: 22:47
 */
public class ProcessorSystemTest {
    ProcessorSystem processorSystem;

    @Before
    public void init() {

        //Task
        Links links = new Links();

        Node task1 = new Node();
        Node task2 = new Node();
        Node task3 = new Node();

        Link link12 = new Link(task1, task2);
        Link link13 = new Link(task1, task3);
        links.add(link12);
        links.add(link13);

        Graph tasks = new Graph(true);
        tasks.addNode(task1);
        tasks.addNode(task2);
        tasks.addNode(task3);
        tasks.addLinks(links);


        //System

        links = new Links();

        Node proc1 = new Node();
        Node proc2 = new Node();

        link12 = new Link(proc1, proc2);
        links.add(link12);

        Graph procs = new Graph(false);
        procs.addNode(proc1);
        procs.addNode(proc2);
        procs.addLinks(links);

        processorSystem = new ProcessorSystem(procs, tasks);

        processorSystem.simulationInfo = new SimulationInfo(1,1,1,SimulationInfo.LinkMode.DUPLEX, new SingleFactorQueue(new NodeWeightAscFactorEvaluatorImpl(), tasks.getNodes()));

        processorSystem.scheduler = new Scheduler3Impl();
    }

    @Test
    public void testInitSimulation() {
        processorSystem.initProcessors();
        assertNotNull(processorSystem.processors);
    }

    @Test
    public void testSimulation() {
        processorSystem.simulation.start();
        List<Executable> events = processorSystem.statistics.events;

    }

}
