package edu.kpi.pzks.gui.ui.panels.GenerateGraph;

import edu.kpi.pzks.gui.ui.MainFrame;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;

/**
 * @author smarx
 */
public class GenerateGraphPanel {
    private JSlider connectivitySlider;
    private JButton genTaskGraphButton;
    private JSlider nodeCountSlider;
    private JSlider minWeightSlider;
    private JSlider maxWeightSlider;
    private JLabel connectivityLabel;
    private JLabel nodeCountLabel;
    private JLabel minWeightLabel;
    private JLabel maxWeightLabel;
    private JPanel mainPanel;
    private MainFrame mainFrame;

    public GenerateGraphPanel(MainFrame mainFrame) {
        this.mainFrame = mainFrame;
        minWeightSlider.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                if(maxWeightSlider.getValue() < minWeightSlider.getValue()) {
                    maxWeightSlider.setValue(minWeightSlider.getValue());
                }
                minWeightLabel.setText(String.valueOf(minWeightSlider.getValue()));
            }
        });
        minWeightLabel.setText(String.valueOf(minWeightSlider.getValue()));

        maxWeightSlider.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                if(maxWeightSlider.getValue() < minWeightSlider.getValue()) {
                    minWeightSlider.setValue(maxWeightSlider.getValue());
                }
                maxWeightLabel.setText(String.valueOf(maxWeightSlider.getValue()));
            }
        });
        maxWeightLabel.setText(String.valueOf(maxWeightSlider.getValue()));

        nodeCountSlider.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                nodeCountLabel.setText(String.valueOf(nodeCountSlider.getValue()));
            }
        });
        nodeCountLabel.setText(String.valueOf(nodeCountSlider.getValue()));

        connectivitySlider.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                connectivityLabel.setText(String.valueOf(getConnectivity()));
            }
        });
        connectivityLabel.setText(String.valueOf(getConnectivity()));
    }

    public void setData(GenerateGraphBean data) {
    }

    public void getData(GenerateGraphBean data) {
    }

    public boolean isModified(GenerateGraphBean data) {
        return false;
    }

    public JButton getGenTaskGraphButton() {
        return genTaskGraphButton;
    }

    public int getNumberOfNodes() {
        return nodeCountSlider.getValue();
    }

    public int getMinNodeWeight() {
        return minWeightSlider.getValue();
    }

    public int getMaxNodeWeight() {
        return maxWeightSlider.getValue();
    }

    public double getConnectivity() {
        return (double)connectivitySlider.getValue()/100;
    }

    public JPanel getMainPanel() {
        return mainPanel;
    }
}
