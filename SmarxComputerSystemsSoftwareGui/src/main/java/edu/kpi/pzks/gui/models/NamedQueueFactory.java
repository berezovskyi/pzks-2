package edu.kpi.pzks.gui.models;

import edu.kpi.pzks.core.model.Graph;
import edu.kpi.pzks.core.model.Links;
import edu.kpi.pzks.core.model.Node;
import edu.kpi.pzks.core.queue.DoubleFactorQueue;
import edu.kpi.pzks.core.queue.SingleFactorQueue;
import edu.kpi.pzks.core.queue.factors.impl.ConnectivityDescFactorEvaluatorImpl;
import edu.kpi.pzks.core.queue.factors.impl.CriticalPathByNumberOfNodesAscFactorEvaluatorImpl;
import edu.kpi.pzks.core.queue.factors.impl.CriticalPathByNumberOfNodesDescFactorEvaluatorImpl;
import edu.kpi.pzks.core.queue.factors.impl.WeightedCriticalFactorEvaluatorImpl;
import edu.kpi.pzks.gui.ui.MainFrame;

import java.util.Set;

/**
 * @author smarx
 */
public class NamedQueueFactory {

    public static NamedQueue buildQueue1(MainFrame frame) {
        Graph graph = frame.getTaskPanel().getGraphView().getGraph();
        Links links = graph.getLinks();
        Set<Node> nodes = graph.getNodes();

        return new NamedQueue(new SingleFactorQueue(new WeightedCriticalFactorEvaluatorImpl(nodes, links), nodes),
                              "Алгоритм №1");
    }

    public static NamedQueue buildQueue4(MainFrame frame) {
        Graph graph = frame.getTaskPanel().getGraphView().getGraph();
        Links links = graph.getLinks();
        Set<Node> nodes = graph.getNodes();

        return new NamedQueue(new DoubleFactorQueue(new CriticalPathByNumberOfNodesDescFactorEvaluatorImpl(nodes, links),
                                                    new ConnectivityDescFactorEvaluatorImpl(nodes, links),
                                                    nodes),
                              "Алгоритм №4");
    }

    public static NamedQueue buildQueue9(MainFrame frame) {
        Graph graph = frame.getTaskPanel().getGraphView().getGraph();
        Links links = graph.getLinks();
        Set<Node> nodes = graph.getNodes();

        return new NamedQueue(new SingleFactorQueue(new CriticalPathByNumberOfNodesAscFactorEvaluatorImpl(nodes, links), nodes),
                              "Алгоритм №9");
    }
}
