package edu.kpi.pzks.gui.models;

import edu.kpi.pzks.core.queue.Queue;

/**
 * @author smarx
 */
public class NamedQueue {
    private Queue queue;
    private String name;

    public NamedQueue(Queue queue, String name) {
        this.queue = queue;
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

    public Queue getQueue() {
        return queue;
    }
}
