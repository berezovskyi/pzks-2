package edu.kpi.pzks.gui.ui.actions;

import edu.kpi.pzks.core.factory.GraphFactory;
import edu.kpi.pzks.core.model.Graph;
import edu.kpi.pzks.gui.layout.CircleLayout;
import edu.kpi.pzks.gui.modelview.GraphView;
import edu.kpi.pzks.gui.modelview.impl.GraphViewImpl;
import edu.kpi.pzks.gui.ui.MainFrame;
import edu.kpi.pzks.gui.ui.panels.GenerateGraph.GenerateGraphPanel;
import edu.kpi.pzks.gui.ui.panels.GraphGeneratorPanel;
import edu.kpi.pzks.gui.ui.panels.GraphPanel;
import edu.kpi.pzks.gui.ui.panels.QueueUIBean;
import edu.kpi.pzks.gui.ui.panels.QueueUIPanel;
import edu.kpi.pzks.gui.utils.STRINGS;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author smarx
 */
public class GraphGenerateAction extends AbstractAction {
    private final MainFrame mainFrame;
    private JFrame frame;

    public GraphGenerateAction(MainFrame mainFrame) {
        this.mainFrame = mainFrame;
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        frame = new JFrame("GenerateGraphPanel");
        final GenerateGraphPanel panel = new GenerateGraphPanel(mainFrame);
        frame.setContentPane(panel.getMainPanel());
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.pack();
        frame.setLocationRelativeTo(mainFrame);
        frame.setTitle("Генерация графа задач");
        frame.setAlwaysOnTop(true);
        frame.setVisible(true);

        panel.getGenTaskGraphButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                GraphPanel graphPanel = mainFrame.getTaskPanel();
                int numberOfNodes = panel.getNumberOfNodes();
                int minNodeWeight = panel.getMinNodeWeight();
                int maxNodeWeight = panel.getMaxNodeWeight();
                double connectivity = panel.getConnectivity();
                Graph generatedGraph = GraphFactory.newGraph(numberOfNodes, minNodeWeight, maxNodeWeight, connectivity);
                GraphView graphView = GraphViewImpl.createView(generatedGraph);
                Dimension prefSize = graphPanel.getSize();
                graphView.setBounds(new Rectangle(0, 0, (int) prefSize.getWidth(), (int) prefSize.getHeight()));
                graphView.layout(new CircleLayout());
                graphPanel.setGraphView(graphView);
                graphPanel.checkGraphIsValid();
                graphPanel.checkSize();
                graphPanel.repaint();
                frame.dispose();
            }
        });
    }
}
