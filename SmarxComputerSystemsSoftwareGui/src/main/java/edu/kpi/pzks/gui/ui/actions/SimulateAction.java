package edu.kpi.pzks.gui.ui.actions;

import edu.kpi.pzks.core.model.Graph;
import edu.kpi.pzks.gui.ui.MainFrame;
import edu.kpi.pzks.gui.ui.panels.QueueUIBean;
import edu.kpi.pzks.gui.ui.panels.QueueUIPanel;
import edu.kpi.pzks.gui.ui.panels.simulate.SimulateDialog;

import javax.swing.*;
import java.awt.event.ActionEvent;

/**
 * @author smarx
 */
public class SimulateAction extends AbstractAction {
    private MainFrame mainFrame;

    public SimulateAction(MainFrame mainFrame) {
        this.mainFrame = mainFrame;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JDialog simulateDialog = new SimulateDialog(mainFrame);
        simulateDialog.pack();
        simulateDialog.setLocationRelativeTo(mainFrame);
        simulateDialog.setTitle("Параметры моделирования");
//        simulateDialog.setAlwaysOnTop(true);
        simulateDialog.setVisible(true);
    }
}
