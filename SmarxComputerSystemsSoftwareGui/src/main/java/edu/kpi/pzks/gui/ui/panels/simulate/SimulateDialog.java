package edu.kpi.pzks.gui.ui.panels.simulate;

import edu.kpi.pzks.core.assign.ProcessorSystem;
import edu.kpi.pzks.core.assign.Scheduler;
import edu.kpi.pzks.core.assign.SimulationInfo;
import edu.kpi.pzks.core.assign.impl.Scheduler3Impl;
import edu.kpi.pzks.core.assign.impl.Scheduler6Impl;
import edu.kpi.pzks.core.model.Graph;
import edu.kpi.pzks.core.queue.Queue;
import edu.kpi.pzks.gui.models.NamedQueue;
import edu.kpi.pzks.gui.models.NamedQueueFactory;
import edu.kpi.pzks.gui.ui.MainFrame;
import edu.kpi.pzks.gui.ui.panels.GanttDiagramPanel;

import javax.swing.*;
import java.awt.event.*;

public class SimulateDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JSpinner channelCount;
    private JCheckBox isDuplex;
    private JComboBox queue;
    private JComboBox scheduler;
    private MainFrame mainFrame;

    private SimulateDialog() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        channelCount.setValue(1);

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        buttonOK.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                /*JOptionPane.showMessageDialog(null, String.format("Q: %s, S: %s", queue.getSelectedItem(),
                                                                  scheduler.getSelectedItem()));*/
            }
        });
    }

    public SimulateDialog(MainFrame mainFrame) {
        this();

        this.mainFrame = mainFrame;

        initQueue();
        initScheduler();
    }

    private void initScheduler() {
        this.scheduler.addItem(new Scheduler3Impl());
        Scheduler6Impl scheduler6 = new Scheduler6Impl();
        this.scheduler.addItem(scheduler6);

        this.scheduler.setSelectedItem(scheduler6);
    }

    private void initQueue() {
        this.queue.addItem(NamedQueueFactory.buildQueue1(mainFrame));
        this.queue.addItem(NamedQueueFactory.buildQueue4(mainFrame));
        this.queue.addItem(NamedQueueFactory.buildQueue9(mainFrame));
    }

    private void onOK() {
        Graph systemGraph = mainFrame.getSystemPanel().getGraphView().getGraph();
        Graph taskGraph = mainFrame.getTaskPanel().getGraphView().getGraph();
        Queue queueGenerator = ((NamedQueue) queue.getSelectedItem()).getQueue();

        int nodePerformance = 1;
        int bandWidth = 1;

        int channelsCount = (int) channelCount.getValue();
        SimulationInfo.LinkMode linkDuplexMode = isDuplex.isSelected() ?
                SimulationInfo.LinkMode.DUPLEX :
                SimulationInfo.LinkMode.HALF_DUPLEX;

        ProcessorSystem system = new ProcessorSystem(systemGraph, taskGraph);
        system.simulationInfo = new SimulationInfo(nodePerformance, channelsCount, bandWidth, linkDuplexMode,
                                                   queueGenerator);
        system.scheduler = (Scheduler) scheduler.getSelectedItem();

        system.simulation.start();

        ProcessorSystem.Statistics events = system.simulation.collectStatistics();

        GanttDiagramPanel chart = new GanttDiagramPanel(events);

        JFrame chartFrame = new JFrame();
        chartFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        chartFrame.setLocationRelativeTo(mainFrame);
        chartFrame.setTitle("Диаграмма Ганта");
        chartFrame.setVisible(true);
        chartFrame.setContentPane(chart.rootPanel);
        chartFrame.pack();
        dispose();
    }

    private void onCancel() {
        // add your code here if necessary
        dispose();
    }
}
