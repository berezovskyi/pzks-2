package edu.kpi.pzks.gui.ui.panels;

import edu.kpi.pzks.core.assign.*;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.entity.CategoryItemEntity;
import org.jfree.chart.entity.EntityCollection;
import org.jfree.chart.labels.CategoryItemLabelGenerator;
import org.jfree.chart.labels.ItemLabelAnchor;
import org.jfree.chart.labels.ItemLabelPosition;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.CategoryItemRendererState;
import org.jfree.chart.renderer.category.GanttRenderer;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.IntervalCategoryDataset;
import org.jfree.data.gantt.GanttCategoryDataset;
import org.jfree.data.gantt.Task;
import org.jfree.data.gantt.TaskSeries;
import org.jfree.data.gantt.TaskSeriesCollection;
import org.jfree.data.time.SimpleTimePeriod;
import org.jfree.text.TextUtilities;
import org.jfree.ui.RectangleEdge;
import org.jfree.ui.TextAnchor;

import java.awt.*;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Author: snoop Date: 13.04.13 Time: 15:04
 */
public class GanttDiagramPanel {

    private int totalTime;
    private List<Processor> processors;
    private int channels;
    public ChartPanel rootPanel;

    public GanttDiagramPanel(ProcessorSystem.Statistics stats) {
        totalTime = stats.totalExecutionTime;
        processors = stats.processors;
        channels = stats.channels;
        rootPanel = new ChartPanel(createChart(stats.events));
        rootPanel.setPreferredSize(new java.awt.Dimension(700, 500));
    }

    /**
     * Here you can modify appearance of your chart
     * Better consult JFreeChart documentation or ask me :)
     *
     * @param events for dataset
     * @return chart for panel
     */
    protected JFreeChart createChart(List<Executable> events) {

        IntervalCategoryDataset dataset = createDataset(events);
        final JFreeChart chart = ChartFactory
                .createGanttChart("Общее время выполнения: " + (totalTime - 1),  // chart title
                                  "Процессоры",              // domain axis label
                                  "Время",              // range axis label
                                  dataset,             // data
                                  true,                // include legend
                                  true,                // tooltips
                                  false                // urls
                                 );
        DateAxis axis = (DateAxis) chart.getCategoryPlot().getRangeAxis();
        axis.setDateFormatOverride(new SimpleDateFormat("S"));

        CategoryPlot plot = (CategoryPlot) chart.getPlot();
        MyGanttRenderer renderer = new MyGanttRenderer();
        plot.setRenderer(renderer);
        renderer.setBaseItemLabelGenerator(getLabelStyle());
        renderer.setBaseItemLabelsVisible(true);
        renderer.setBaseItemLabelPaint(Color.BLACK);
        renderer.setBasePositiveItemLabelPosition(
                new ItemLabelPosition(ItemLabelAnchor.INSIDE6, TextAnchor.BOTTOM_CENTER));

        return chart;
    }

    class SubTaskCategoryItemLabelGenerator implements CategoryItemLabelGenerator {
        @Override
        public String generateRowLabel(CategoryDataset dataset, int row) {
            return "Row  " + row;
        }

        @Override
        public String generateColumnLabel(CategoryDataset dataset, int column) {
            return "Col " + column;
        }

        @Override
        public String generateLabel(CategoryDataset dataset, int row, int column) {
            //never called
            return "" + row + ":" + column;
        }

        public String generateLabel(CategoryDataset dataset, int row, int column, int subinterval) {
            TaskSeriesCollection data = (TaskSeriesCollection) dataset;
            Task task = data.getSeries(row).get(column);
            return task.getSubtask(subinterval).getDescription();
        }
    }

    private CategoryItemLabelGenerator getLabelStyle() {
        return new SubTaskCategoryItemLabelGenerator();
    }

    /**
     * Transform our events to a dataset.
     *
     * @return The dataset.
     */
    protected IntervalCategoryDataset createDataset(List<Executable> events) {
        TaskSeriesCollection collection = new TaskSeriesCollection();


        TaskSeries processorWork = new TaskSeries("Время выполнения");
        collection.add(processorWork);

        final List<TaskSeries> channelWork = new ArrayList<>(channels);


        Map<Processor, Task> taskProcessorMapProc = new HashMap<>();
        Map<Processor, List<Task>> taskProcessorMapChan = new HashMap<>();

        for (int i = 0; i < channels; i++) {
            channelWork.add(new TaskSeries("Время пересылок: канал " + i));
            collection.add(channelWork.get(i));
        }


        for (Processor processor : processors) {
            Task task1 = new Task("P " + processor.getId(), new SimpleTimePeriod(0, totalTime));
            taskProcessorMapProc.put(processor, task1);
            task1.addSubtask(new Task("", new SimpleTimePeriod(0, 0)));

            processorWork.add(task1);

            List<Task> channelsTasks = new ArrayList<>(channels);
            for (int i = 0; i < channels; i++) {
                Task task = new Task("P " + processor.getId(), new SimpleTimePeriod(0, totalTime));

                task.addSubtask(new Task("", new SimpleTimePeriod(0, 0)));
                channelsTasks.add(task);
                channelWork.get(i).add(task);
            }

            taskProcessorMapChan.put(processor, channelsTasks);

        }

        for (Executable event : events) {
            if (event instanceof ProcTask) {
                taskProcessorMapProc.get(event.getProcessor()).addSubtask(
                        new Task(event.toString(), new SimpleTimePeriod(event.getStartTime(), event.getEndTime())));
            }
            else {
                taskProcessorMapChan.get(event.getProcessor()).get(((ProcTransfer) event).getChannel()).addSubtask(
                        new Task(event.toString(), new SimpleTimePeriod(event.getStartTime(), event.getEndTime())));
            }
        }
        return collection;
    }


    public class MyGanttRenderer extends GanttRenderer {
        private transient Paint completePaint;
        private transient Paint incompletePaint;
        private double startPercent;
        private double endPercent;

        public MyGanttRenderer() {
            super();
            setIncludeBaseInRange(false);
            this.completePaint = Color.green;
            this.incompletePaint = Color.red;
            this.startPercent = 0.35;
            this.endPercent = 0.65;
        }

        protected void drawTasks(Graphics2D g2, CategoryItemRendererState state, Rectangle2D dataArea,
                                 CategoryPlot plot, CategoryAxis domainAxis, ValueAxis rangeAxis,
                                 GanttCategoryDataset dataset, int row, int column) {

            int count = dataset.getSubIntervalCount(row, column);
            if (count == 0) {
                drawTask(g2, state, dataArea, plot, domainAxis, rangeAxis, dataset, row, column);
            }

            for (int subinterval = 0; subinterval < count; subinterval++) {

                RectangleEdge rangeAxisLocation = plot.getRangeAxisEdge();

                // value 0
                Number value0 = dataset.getStartValue(row, column, subinterval);
                if (value0 == null) {
                    return;
                }
                double translatedValue0 = rangeAxis.valueToJava2D(value0.doubleValue(), dataArea, rangeAxisLocation);

                // value 1
                Number value1 = dataset.getEndValue(row, column, subinterval);
                if (value1 == null) {
                    return;
                }
                double translatedValue1 = rangeAxis.valueToJava2D(value1.doubleValue(), dataArea, rangeAxisLocation);

                if (translatedValue1 < translatedValue0) {
                    double temp = translatedValue1;
                    translatedValue1 = translatedValue0;
                    translatedValue0 = temp;
                }

                double rectStart = calculateBarW0(plot, plot.getOrientation(), dataArea, domainAxis, state, row,
                                                  column);
                double rectLength = Math.abs(translatedValue1 - translatedValue0);
                double rectBreadth = state.getBarWidth();

                // DRAW THE BARS...
                Rectangle2D bar = null;

                if (plot.getOrientation() == PlotOrientation.HORIZONTAL) {
                    bar = new Rectangle2D.Double(translatedValue0, rectStart, rectLength, rectBreadth);
                }
                else if (plot.getOrientation() == PlotOrientation.VERTICAL) {
                    bar = new Rectangle2D.Double(rectStart, translatedValue0, rectBreadth, rectLength);
                }

                Rectangle2D completeBar = null;
                Rectangle2D incompleteBar = null;
                Number percent = dataset.getPercentComplete(row, column, subinterval);
                double start = getStartPercent();
                double end = getEndPercent();
                if (percent != null) {
                    double p = percent.doubleValue();
                    if (plot.getOrientation() == PlotOrientation.HORIZONTAL) {
                        completeBar = new Rectangle2D.Double(translatedValue0, rectStart + start * rectBreadth,
                                                             rectLength * p, rectBreadth * (end - start));
                        incompleteBar = new Rectangle2D.Double(translatedValue0 + rectLength * p,
                                                               rectStart + start * rectBreadth, rectLength * (1 - p),
                                                               rectBreadth * (end - start));
                    }
                    else if (plot.getOrientation() == PlotOrientation.VERTICAL) {
                        completeBar = new Rectangle2D.Double(rectStart + start * rectBreadth,
                                                             translatedValue0 + rectLength * (1 - p),
                                                             rectBreadth * (end - start), rectLength * p);
                        incompleteBar = new Rectangle2D.Double(rectStart + start * rectBreadth, translatedValue0,
                                                               rectBreadth * (end - start), rectLength * (1 - p));
                    }

                }

                Paint seriesPaint = getItemPaint(row, column);
                g2.setPaint(seriesPaint);
                g2.fill(bar);

                if (completeBar != null) {
                    g2.setPaint(getCompletePaint());
                    g2.fill(completeBar);
                }
                if (incompleteBar != null) {
                    g2.setPaint(getIncompletePaint());
                    g2.fill(incompleteBar);
                }
                if (isDrawBarOutline() && state.getBarWidth() > BAR_OUTLINE_WIDTH_THRESHOLD) {
                    g2.setStroke(getItemStroke(row, column));
                    g2.setPaint(getItemOutlinePaint(row, column));
                    g2.draw(bar);
                }

                SubTaskCategoryItemLabelGenerator generator = (SubTaskCategoryItemLabelGenerator) getItemLabelGenerator(
                        row, column);
                if (generator != null && isItemLabelVisible(row, column)) {
                    drawItemLabel(g2, dataset, row, column, subinterval, plot, generator, bar, false);
                }

                // collect entity and tool tip information...
                if (state.getInfo() != null) {
                    EntityCollection entities = state.getEntityCollection();
                    if (entities != null) {
                        String tip = null;
                        if (getToolTipGenerator(row, column) != null) {
                            tip = getToolTipGenerator(row, column).generateToolTip(dataset, row, column);
                        }
                        String url = null;
                        if (getItemURLGenerator(row, column) != null) {
                            url = getItemURLGenerator(row, column).generateURL(dataset, row, column);
                        }
                        CategoryItemEntity entity = new CategoryItemEntity(bar, tip, url, dataset,
                                                                           dataset.getRowKey(row),
                                                                           dataset.getColumnKey(column));
                        entities.add(entity);
                    }
                }
            }
        }

        protected void drawItemLabel(Graphics2D g2, GanttCategoryDataset data, int row, int column, int subinterval,
                                     CategoryPlot plot, SubTaskCategoryItemLabelGenerator generator, Rectangle2D bar,
                                     boolean negative) {

            String label = generator.generateLabel(data, row, column, subinterval);
            if (label == null) {
                return;  // nothing to do
            }
            Font labelFont = getItemLabelFont(row, column);
            g2.setFont(labelFont);
            Paint paint = getItemLabelPaint(row, column);
            g2.setPaint(paint);

            // find out where to place the label...
            ItemLabelPosition position;
            if (!negative) {
                position = getPositiveItemLabelPosition(row, column);
            }
            else {
                position = getNegativeItemLabelPosition(row, column);
            }

            // work out the label anchor point...
            double x3 = bar.getCenterX();
            double y3 = bar.getCenterY();

            Point2D anchorPoint = new Point2D.Double(x3, y3);

            TextUtilities.drawRotatedString(label, g2, (float) anchorPoint.getX(), (float) anchorPoint.getY(),
                                            position.getTextAnchor(), position.getAngle(),
                                            position.getRotationAnchor());
        }
    }
}
