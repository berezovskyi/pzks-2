package edu.kpi.pzks.gui.actions;

import edu.kpi.pzks.core.model.Graph;
import edu.kpi.pzks.gui.ui.MainFrame;
import edu.kpi.pzks.gui.ui.popups.SimulationSettingsFrame;

import javax.swing.*;
import java.awt.event.ActionEvent;

/**
 * @author smarx
 */
public class SimulateAction extends AbstractAction {

    private MainFrame mainFrame;

    public SimulateAction(MainFrame mainFrame) {
        this.mainFrame = mainFrame;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Graph taskGraph = mainFrame.getTaskPanel().getGraphView().getGraph();
        Graph systemGraph = mainFrame.getSystemPanel().getGraphView().getGraph();
        SimulationSettingsFrame simulationFrame = new SimulationSettingsFrame(taskGraph, systemGraph);
        simulationFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        simulationFrame.pack();
        simulationFrame.setLocationRelativeTo(mainFrame);
        simulationFrame.setTitle("Моделирование");
        simulationFrame.setAlwaysOnTop(true);
        simulationFrame.setVisible(true);
    }
}
