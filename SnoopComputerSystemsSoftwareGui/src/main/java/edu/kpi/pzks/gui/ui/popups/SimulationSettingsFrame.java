package edu.kpi.pzks.gui.ui.popups;

import edu.kpi.pzks.core.assign.ProcessorSystem;
import edu.kpi.pzks.core.assign.Scheduler;
import edu.kpi.pzks.core.assign.SimulationInfo;
import edu.kpi.pzks.core.assign.impl.Scheduler3Impl;
import edu.kpi.pzks.core.assign.impl.Scheduler6Impl;
import edu.kpi.pzks.core.model.Graph;
import edu.kpi.pzks.core.model.Link;
import edu.kpi.pzks.core.model.Node;
import edu.kpi.pzks.core.queue.*;
import edu.kpi.pzks.core.queue.factors.impl.CriticalPathByLengthForAllNodesDescFactorEvaluatorImpl;
import edu.kpi.pzks.core.queue.factors.impl.CriticalPathByTimeForAllNodesDescFactorEvaluatorImpl;
import edu.kpi.pzks.core.queue.factors.impl.NodeWeightAscFactorEvaluatorImpl;
import edu.kpi.pzks.gui.ui.panels.GanttDiagramPanel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collection;

/**
 * @author Snoop
 */
public class SimulationSettingsFrame extends JFrame {
    private JTextField channelsCountField;

    public JPanel rootPanel;
    private Graph taskGraph;

    private final String scheduler3 = "Планировщик 3";
    private final String scheduler6 = "Планировщик 6";


    private final String queue3 = "Очередь 3";
    private final String queue6 = "Очередь 6";
    private final String queue15 = "Очередь 15";


    private Graph systemGraph;

    Scheduler scheduler;
    private int nodePerformance;
    private int bandwidth;
    JRadioButton duplexRadio;
    JRadioButton halfDuplexRadio;

    private Queue queueGenerator;


    public SimulationSettingsFrame(Graph taskGraph, Graph systemGraph) {
        this.taskGraph = taskGraph;
        this.systemGraph = systemGraph;
        initComponents();
        setContentPane(rootPanel);

    }

    private void initComponents() {
        this.rootPanel = new JPanel();
        rootPanel.setLayout(new BoxLayout(rootPanel, BoxLayout.Y_AXIS));

        JLabel l = new JLabel("Количество каналов");
        rootPanel.add(l);

        this.channelsCountField = new JTextField();
        this.channelsCountField.setFont(new Font("Segoe UI", Font.PLAIN, 13));
        rootPanel.add(this.channelsCountField);


        rootPanel.add(Box.createRigidArea(new Dimension(0,5)));

        l = new JLabel("Тип каналов");
        rootPanel.add(l);

        duplexRadio = new JRadioButton("Дуплекс");
        halfDuplexRadio = new JRadioButton("Полудуплекс");
        duplexRadio.setSelected(true);

        ButtonGroup radios = new ButtonGroup();
        radios.add(duplexRadio);
        radios.add(halfDuplexRadio);

        rootPanel.add(duplexRadio);
        rootPanel.add(halfDuplexRadio);


        rootPanel.add(Box.createRigidArea(new Dimension(0,5)));

        JLabel l2 = new JLabel("Планировщик");
        rootPanel.add(l2);
        l2.setAlignmentX(Component.LEFT_ALIGNMENT);
        addSchedulerSelector(rootPanel);

        rootPanel.add(Box.createRigidArea(new Dimension(0,5)));

        addQueueSelector(rootPanel);
        rootPanel.add(Box.createVerticalGlue());

        if(systemGraph.getNodes().iterator().hasNext()) {
            Node firstNode = systemGraph.getNodes().iterator().next();
            this.nodePerformance = firstNode.getWeight();
            JLabel label = new JLabel("Производительность процессоров: " + firstNode.getWeight());
            rootPanel.add(label);
        } else {
            JLabel label = new JLabel("Нет процессоров? А где тогда запускать???");
            rootPanel.add(label);
        }

        rootPanel.add(Box.createRigidArea(new Dimension(0,5)));

        if(systemGraph.getLinks().iterator().hasNext()) {
            Link firstLink = systemGraph.getLinks().iterator().next();
            this.bandwidth = firstLink.getWeight();
            JLabel label = new JLabel("Пропускная способность каналов: " + firstLink.getWeight());
            rootPanel.add(label);
        } else {
            JLabel label = new JLabel("Нет соединений? А как тогда передавать???");
            rootPanel.add(label);
        }



        rootPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));



        JButton start = new JButton(new StartSimulationAction());

        start.setText("Начать моделирование");
        start.setAlignmentX(Component.LEFT_ALIGNMENT);
        //rootPanel.add(Box.createHorizontalGlue());
        rootPanel.add(start);
        JButton cancel = new JButton(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                SimulationSettingsFrame.this.setVisible(false);
            }
        });

        cancel.setText("Отмена");
        cancel.setAlignmentX(Component.LEFT_ALIGNMENT);
        rootPanel.add(cancel);

    }

    private void addSchedulerSelector(Container parent) {


        JComboBox<String> comboBox = new JComboBox<>(new String[]{this.scheduler3, scheduler6});
        comboBox.addActionListener(new SchedulerTypeChangedListener());
        comboBox.setMaximumSize(new Dimension(1000, 20));
        comboBox.setAlignmentX(Component.LEFT_ALIGNMENT);
        parent.add(comboBox);
        comboBox.setSelectedIndex(0);

    }

    private void addQueueSelector(Container parent) {


        JComboBox<String> comboBox = new JComboBox<>(new String[]{this.queue3, queue6, queue15});
        comboBox.addActionListener(new QueueTypeChangedListener());
        comboBox.setMaximumSize(new Dimension(1000, 20));
        comboBox.setAlignmentX(Component.LEFT_ALIGNMENT);
        parent.add(comboBox);
        comboBox.setSelectedIndex(0);
    }


    class SchedulerTypeChangedListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            JComboBox comboBox = (JComboBox) e.getSource();
            switch ((String)comboBox.getSelectedItem()) {
                case scheduler3:
                    scheduler = new Scheduler3Impl();
                    break;
                case scheduler6:
                    scheduler = new Scheduler6Impl();
                    break;
            }
        }
    }

    class QueueTypeChangedListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            JComboBox comboBox = (JComboBox) e.getSource();
            switch ((String)comboBox.getSelectedItem()) {
                case queue3:
                    queueGenerator = new SingleFactorQueue(new CriticalPathByTimeForAllNodesDescFactorEvaluatorImpl(taskGraph.getNodes(), taskGraph.getLinks()), taskGraph.getNodes());
                    break;
                case queue6:
                    queueGenerator = new SingleFactorQueue(new CriticalPathByLengthForAllNodesDescFactorEvaluatorImpl(taskGraph.getNodes(), taskGraph.getLinks()), taskGraph.getNodes());
                    break;
                case queue15:
                    queueGenerator = new SingleFactorQueue(new NodeWeightAscFactorEvaluatorImpl(), taskGraph.getNodes());
                    break;
            }
        }
    }

    class StartSimulationAction extends AbstractAction {

        @Override
        public void actionPerformed(ActionEvent e) {
            try {
            validateFields();
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(SimulationSettingsFrame.this,
                        "Ошибка в настройках миделирования.",
                        ex.getMessage(),
                        JOptionPane.ERROR_MESSAGE);
            }
            ProcessorSystem system = new ProcessorSystem(systemGraph, taskGraph);
            system.simulationInfo = new SimulationInfo(
                    SimulationSettingsFrame.this.nodePerformance,
                    Integer.parseInt(SimulationSettingsFrame.this.channelsCountField.getText()),
                    SimulationSettingsFrame.this.bandwidth,
                    SimulationSettingsFrame.this.duplexRadio.isSelected()? SimulationInfo.LinkMode.DUPLEX: SimulationInfo.LinkMode.HALF_DUPLEX,
                    queueGenerator);

            system.scheduler = SimulationSettingsFrame.this.scheduler;

            system.simulation.start();

            ProcessorSystem.Statistics events = system.simulation.collectStatistics();
            GanttDiagramPanel chart = new GanttDiagramPanel(events);

            JFrame chartFrame = new JFrame();
            chartFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            chartFrame.setLocationRelativeTo(SimulationSettingsFrame.this);
            chartFrame.setTitle("Диаграмма Ганта");
            chartFrame.setAlwaysOnTop(true);
            chartFrame.setVisible(true);
            chartFrame.setContentPane(chart.rootPanel);
            chartFrame.pack();
            SimulationSettingsFrame.this.setVisible(false);


        }


    }

    private void validateFields() {
        try{
            Integer.parseInt(SimulationSettingsFrame.this.channelsCountField.getText());
        } catch (NumberFormatException e) {
            throw new NumberFormatException("Число каналов должно быть целым числом!");
        }
    }
}
