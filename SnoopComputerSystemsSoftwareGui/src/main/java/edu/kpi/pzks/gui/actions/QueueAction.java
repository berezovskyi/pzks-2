package edu.kpi.pzks.gui.actions;

import edu.kpi.pzks.core.model.Graph;
import edu.kpi.pzks.gui.ui.popups.QueueInfoPanel;
import edu.kpi.pzks.gui.ui.MainFrame;

import javax.swing.*;
import java.awt.event.ActionEvent;

/**
 * @author smarx
 */
public class QueueAction extends AbstractAction{

    private MainFrame mainFrame;

    public QueueAction(MainFrame mainFrame) {
        this.mainFrame = mainFrame;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JFrame frame = new JFrame();
        Graph graph = mainFrame.getTaskPanel().getGraphView().getGraph();
        QueueInfoPanel queueUIPanel = new QueueInfoPanel(graph);
        frame.setContentPane(queueUIPanel.rootPanel);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.pack();
        frame.setLocationRelativeTo(mainFrame);
        frame.setTitle("Генерация очередей");
        frame.setAlwaysOnTop(true);
        frame.setVisible(true);
    }
}
