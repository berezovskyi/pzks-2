package edu.kpi.pzks.gui.ui.popups;

import edu.kpi.pzks.core.model.Graph;
import edu.kpi.pzks.core.queue.Queue;
import edu.kpi.pzks.core.queue.QueuedNode;
import edu.kpi.pzks.core.queue.SingleFactorQueue;
import edu.kpi.pzks.core.queue.factors.impl.CriticalPathByLengthForAllNodesDescFactorEvaluatorImpl;
import edu.kpi.pzks.core.queue.factors.impl.CriticalPathByTimeForAllNodesDescFactorEvaluatorImpl;
import edu.kpi.pzks.core.queue.factors.impl.NodeWeightAscFactorEvaluatorImpl;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collection;

/**
 * @author Snoop
 */
public class QueueInfoPanel {

    private JTextArea queueInfoTextPane;
    public JPanel rootPanel;
    private Graph graph;

    private final String queue3 = "Queue 3";
    private final String queue6 = "Queue 6";
    private final String queue15 = "Queue 15";


    public QueueInfoPanel(Graph graph) {
        this.graph = graph;
        initComponents();

    }

    private void initComponents() {
        this.rootPanel = new JPanel(new BorderLayout());

        addTextPane(rootPanel);

        addToolBar(rootPanel);

    }

    private void addToolBar(Container rootPanel) {
        JPanel toolBar = new JPanel();
        toolBar.setLayout(new BoxLayout(toolBar, BoxLayout.Y_AXIS));

        JLabel label = new JLabel("Выбор очереди");
        toolBar.add(label);
        label.setAlignmentX(Component.LEFT_ALIGNMENT);

        toolBar.add(Box.createRigidArea(new Dimension(0,5)));
        addQueueSelector(toolBar);
        toolBar.add(Box.createVerticalGlue());
        toolBar.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));

        rootPanel.add(toolBar, BorderLayout.WEST);
    }

    private void addQueueSelector(Container parent) {


        JComboBox<String> comboBox = new JComboBox<>(new String[]{this.queue3, queue6, queue15});
        comboBox.addActionListener(new QueueTypeChangedListener());
        comboBox.setMaximumSize(new Dimension(1000, 20));
        comboBox.setAlignmentX(Component.LEFT_ALIGNMENT);
        parent.add(comboBox);
        comboBox.setSelectedIndex(0);
    }

    private void addTextPane(Container parent) {
        this.queueInfoTextPane = new JTextArea();
        this.queueInfoTextPane.setFont(new Font("Segoe UI", Font.PLAIN, 13));
        this.queueInfoTextPane.setPreferredSize(new Dimension(300, 300));
        parent.add(this.queueInfoTextPane, BorderLayout.CENTER);
    }

    class QueueTypeChangedListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            JComboBox comboBox = (JComboBox) e.getSource();
            queueInfoTextPane.setText("");
            Queue queueGenerator = null;
            switch ((String)comboBox.getSelectedItem()) {
                case queue3:
                    queueGenerator = new SingleFactorQueue(new CriticalPathByTimeForAllNodesDescFactorEvaluatorImpl(graph.getNodes(), graph.getLinks()), graph.getNodes());
                    break;
                case queue6:
                    queueGenerator = new SingleFactorQueue(new CriticalPathByLengthForAllNodesDescFactorEvaluatorImpl(graph.getNodes(), graph.getLinks()), graph.getNodes());
                    break;
                case queue15:
                    queueGenerator = new SingleFactorQueue(new NodeWeightAscFactorEvaluatorImpl(), graph.getNodes());
                    break;
            }
            if(queueGenerator != null) {
                Collection<QueuedNode> nodes = queueGenerator.evaluate();
                for(QueuedNode queuedNode : nodes) {
                    queueInfoTextPane.append(queuedNode.toString() + "\n");
                }
            }

        }
    }
}
