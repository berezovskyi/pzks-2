﻿Param (
    [switch]$build = $false
)

$base_dir = Split-Path -parent $MyInvocation.MyCommand.Definition
$cwd= $base_dir | Split-Path -parent | Split-Path -parent
$project_dir=$($cwd | Split-Path -Parent)

cd $project_dir

$core_dir = $project_dir + "\ComputerSystemsSoftwareCore"
$gui_dir = $cwd

$pom_core_path = "$core_dir\pom.xml"
$pom_gui_path = "$gui_dir\pom.xml"

$jar_version="1.0"
$schedulers = @(3,6)
$queues = @(1,4,9)

$jar_name="ComputerSystemsSoftwareGui-$jar_version-SNAPSHOT-jar-with-dependencies.jar"
$jar_path="$cwd\target\$jar_name"

$build_core_command="mvn -q -f ""$pom_core_path"" clean install -DskipTests"
$build_gui_command="mvn -q -f ""$pom_gui_path"" clean install -DskipTests assembly:single"

Try {
    If($build -eq $true) {
        Write-Debug "Preparing to build '$build_command'"

        Invoke-Expression $build_core_command
        Invoke-Expression $build_gui_command
    }

    $system_name='5square' # file must be in the same dir as the ps1 script
    $out_basedir = "$base_dir\out\"


    foreach($s in $schedulers) {
        foreach($q in $queues) {
            foreach($nn in @(20, 40, 60, 80)) {
                $output_dir = "$out_basedir\system_name-s$s-p$p-n$nn\"
                if(!(Test-Path $output_dir)){New-Item -ItemType directory -Path $output_dir | Out-Null}
                $run_command="java -jar '$jar_path' -system '$base_dir\$system_name.system' -q $q -s $s -nc 1 -nn $nn -min 10 -max 50 -d -graph '$output_dir'"
                Write-Debug "Preparing to execute '$jar_path'"

                Invoke-Expression $run_command > "$out_basedir\simulation-s$s-q$q-$nn.tex"
            }
        }
    }
}

Catch {
    $error[0]
}

Finally {
    $MyInvocation.MyCommand.Path | Split-Path -parent | cd
}