package edu.kpi.pzks.gui;

import edu.kpi.pzks.core.exceptions.GraphException;
import edu.kpi.pzks.core.model.Graph;
import edu.kpi.pzks.core.model.Link;
import edu.kpi.pzks.core.model.Links;
import edu.kpi.pzks.core.model.Node;
import edu.kpi.pzks.core.model.TYPE;
import edu.kpi.pzks.gui.io.GraphLoader;
import edu.kpi.pzks.gui.io.GraphSaver;
import edu.kpi.pzks.gui.io.impl.XmlGraphLoader;
import edu.kpi.pzks.gui.io.impl.XmlGraphSaver;
import edu.kpi.pzks.gui.modelview.GraphView;
import edu.kpi.pzks.gui.modelview.LinkView;
import edu.kpi.pzks.gui.modelview.NodeView;
import edu.kpi.pzks.gui.modelview.impl.GraphViewImpl;
import edu.kpi.pzks.gui.modelview.impl.LinkViewImpl;
import java.awt.Point;
import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Random;
import java.util.Set;
import org.junit.Test;
import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

/**
 *
 * @author Aloren
 */
public class XmlLoaderSaverTest {

    Random random = new Random();
    int size = 5;
    Set<Node> nodes;
    Set<Link> links;
    Collection<LinkView> linkViews;
    Collection<NodeView> nodeViews;

    @Test
    public void testSave() {
        nodes = getNodes();
        links = getLinks(nodes);
        linkViews = createLinkViewsCollection();
        nodeViews = createNodeViewsCollection();

        String fileName = "test.xml";
        GraphSaver saver = new XmlGraphSaver();
        GraphLoader loader = new XmlGraphLoader();

        File file = new File(fileName);
        if (file.exists()) {
            file.delete();
        }
        try {
            file.createNewFile();
        } catch (IOException ex) {
            fail("Exception while creating new file!");
        }
        GraphView graphView = mock(GraphViewImpl.class);
        Graph graph = mock(Graph.class);
        when(graph.getLinks()).thenReturn(new Links(links));
        when(graph.getNodes()).thenReturn(nodes);
        when(graphView.getGraph()).thenReturn(graph);
        when(graph.isOriented()).thenReturn(Boolean.TRUE);

        when(graphView.getLinkViews()).thenReturn(linkViews);
        when(graphView.getNodeViews()).thenReturn(nodeViews);
        when(graphView.getType()).thenReturn(TYPE.TASK);
        try {
            saver.saveToFile(graphView, file);
        } catch (GraphException ex) {
            fail("Exception while saving graph to file!");
        }
        //TODO add validation
        try {
            GraphView testGraphView = loader.loadFromFile(file);
            checkGraphs(testGraphView, graphView);
        } catch (GraphException ex) {
            fail("Unexpected exception");
        }

        //Comment this line if you want to look through file with saved graph
        file.delete();
    }

    private Set<Node> getNodes() {
        Set<Node> localNodes = new LinkedHashSet<>();
        for (int i = 0; i < size; i++) {
            final int randomWeight = random(1, 100);
            localNodes.add(new Node(randomWeight));
        }
        return localNodes;
    }

    private Set<Link> getLinks(Set<Node> nodes) {
        Set<Link> localLinks = new LinkedHashSet<>();
        Node[] nodesAr = nodes.toArray(new Node[nodes.size()]);
        for (int i = 0; i < size; i++) {
            Node fromNode = nodesAr[i];
            Node toNode = (i + 1 == size) ? nodesAr[0] : nodesAr[i + 1];
            final int randomWeight = random(1, 100);
            localLinks.add(new Link(fromNode, toNode, randomWeight));
        }
        return localLinks;
    }

    private Collection<LinkView> createLinkViewsCollection() {
        Collection<LinkView> linkViews = new LinkedList<>();
        Link[] linksAr = links.toArray(new Link[links.size()]);
        for (int i = 0; i < size; i++) {
            LinkView linkView = mock(LinkViewImpl.class);
            when(linkView.getLink()).thenReturn(linksAr[i]);
            when(linkView.getBendPoint()).thenReturn(new Point(i * 5, i * 5));
            linkViews.add(linkView);
        }
        return linkViews;
    }

    private Collection<NodeView> createNodeViewsCollection() {
        Collection<NodeView> nodeViews = new LinkedList<>();
        Node[] nodesAr = nodes.toArray(new Node[nodes.size()]);
        for (int i = 0; i < size; i++) {
            NodeView nodeView = mock(NodeView.class);
            when(nodeView.getNode()).thenReturn(nodesAr[i]);
            when(nodeView.getUpperLeftCorner()).thenReturn(new Point(i, i));
            nodeViews.add(nodeView);
        }
        return nodeViews;
    }

    private void checkGraphs(GraphView testGraphView, GraphView expectedGraphView) {
        List<NodeView> testNodeViews = new LinkedList<>(testGraphView.getNodeViews());
        List<NodeView> expectedNodeViews = new LinkedList<>(expectedGraphView.getNodeViews());
        assertTrue(testNodeViews.size() == expectedNodeViews.size());

        List<LinkView> testLinkViews = new LinkedList<>(testGraphView.getLinkViews());
        List<LinkView> expectedLinkViews = new LinkedList<>(expectedGraphView.getLinkViews());
        assertTrue(testLinkViews.size() == expectedLinkViews.size());

        Graph testGraph = testGraphView.getGraph();
        Graph expectedGraph = expectedGraphView.getGraph();

        List<Link> testLinks = new LinkedList<>(testGraph.getLinks());
        List<Link> expectedLinks = new LinkedList<>(expectedGraph.getLinks());
        assertTrue(testLinks.size() == expectedLinks.size());
        assertTrue(testLinkViews.size() == testLinks.size());

        List<Node> testNodes = new LinkedList<>(testGraph.getNodes());
        List<Node> expectedNodes = new LinkedList<>(expectedGraph.getNodes());
        assertTrue(testNodes.size() == expectedNodes.size());
        assertTrue(testNodeViews.size() == testNodes.size());

        testNodeViews(testNodeViews, expectedNodeViews);
        testLinkViews(testLinkViews, expectedLinkViews);
        testLinks(testLinks, expectedLinks);
        //TODO add test for nodes

//        for (int i = 0; i < expectedNodes.size(); i++) {
//            Node controlNode = expectedNodes.get(i);
//            for (int j = 0; j < testNodes.size(); j++) {
//            }
//            Node controlNode = expectedNodes.get(i);
//            assertNodesEqual(controlNode, controlNode);
//        }
    }

    private void assertNodesEqual(Node controlNode, Node testNode) {
        if (testNode.getWeight() != controlNode.getWeight()
                && controlNode.getOutputNodes().size() != testNode.getOutputNodes().size()
                && controlNode.getInputNodes().size() != testNode.getInputNodes().size()) {
            fail();
        }
    }

    private static int random(int min, int max) {
        return min + (int) (Math.random() * ((max - min) + 1));
    }

    private void testNodeViews(List<NodeView> testNodeViews, List<NodeView> expectedNodeViews) {
        ListIterator<NodeView> testNodeViewIterator = testNodeViews.listIterator();
        ListIterator<NodeView> expectedNodeViewIterator = expectedNodeViews.listIterator();
        while (testNodeViewIterator.hasNext()) {
            NodeView testNodeView = testNodeViewIterator.next();
            NodeView controlNodeView = expectedNodeViewIterator.next();
            while (!testNodeView.getUpperLeftCorner().equals(controlNodeView.getUpperLeftCorner())
                    && testNodeView.getNode().getWeight() != controlNodeView.getNode().getWeight()) {
                controlNodeView = expectedNodeViewIterator.next();
            }
            expectedNodeViewIterator.remove();
            expectedNodeViewIterator = expectedNodeViews.listIterator();
        }
        assertTrue(expectedNodeViews.isEmpty());
    }

    private void testLinkViews(List<LinkView> testLinkViews, List<LinkView> expectedLinkViews) {
        ListIterator<LinkView> testLinkViewIterator = testLinkViews.listIterator();
        ListIterator<LinkView> expectedLinkViewIterator = expectedLinkViews.listIterator();
        while (testLinkViewIterator.hasNext()) {
            LinkView testLinkView = testLinkViewIterator.next();
            LinkView controlLinkView = expectedLinkViewIterator.next();
            final Link testLink = testLinkView.getLink();
            final Link controlLink = controlLinkView.getLink();
            while (!testLinkView.getBendPoint().equals(controlLinkView.getBendPoint())
                    && testLink.getWeight() != controlLink.getWeight()
                    && testLink.getFromNode().getWeight() != controlLink.getFromNode().getWeight()
                    && testLink.getToNode().getWeight() != controlLink.getToNode().getWeight()) {
                controlLinkView = expectedLinkViewIterator.next();
            }
            expectedLinkViewIterator.remove();
            expectedLinkViewIterator = expectedLinkViews.listIterator();
        }
        assertTrue(expectedLinkViews.isEmpty());
    }

    private void testLinks(List<Link> testLinks, List<Link> expectedLinks) {
        ListIterator<Link> testLinkIterator = testLinks.listIterator();
        ListIterator<Link> expectedLinkIterator = expectedLinks.listIterator();
        while (testLinkIterator.hasNext()) {
            Link testLink = testLinkIterator.next();
            Link controlLink = expectedLinkIterator.next();
            while (testLink.getWeight() != controlLink.getWeight()
                    && testLink.getFromNode().getWeight() != controlLink.getFromNode().getWeight()
                    && testLink.getToNode().getWeight() != controlLink.getToNode().getWeight()) {
                controlLink = expectedLinkIterator.next();
            }
            expectedLinkIterator.remove();
            expectedLinkIterator = expectedLinks.listIterator();
        }
    }
}
