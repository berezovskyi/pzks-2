package edu.kpi.pzks.console;

import edu.kpi.pzks.core.assign.Scheduler;
import edu.kpi.pzks.core.assign.impl.Scheduler3Impl;
import edu.kpi.pzks.core.assign.impl.Scheduler4Impl;
import edu.kpi.pzks.core.assign.impl.Scheduler6Impl;

/**
 *
 * @author Aloren
 */
public class SchedulerFactory {

    public static Scheduler getScheduler(int index) {
        Scheduler scheduler;
        switch (index) {
            case 3:
                scheduler = new Scheduler3Impl();
                break;
            case 4:
                scheduler = new Scheduler4Impl();
                break;
            case 6:
                scheduler = new Scheduler6Impl();
                break;
            default:
                throw new IllegalArgumentException("Scheduler variant " + index + " is not implemented!");
        }
        return scheduler;
    }
}
