package edu.kpi.pzks.console;

import java.util.Arrays;

/**
 *
 * @author Aloren
 */
public class ParametersCounter {

    public int[] totalExecutionTime;
    public double[] accelarationCoeff;
    public double[] efficiencyCoeff;
    public double[] algorithmEfficiencyCoeff;
    public int[] criticalPath;
    private final int variants;

    public ParametersCounter(int variants) {
        this.variants = variants;
        this.totalExecutionTime = new int[variants];
        this.accelarationCoeff = new double[variants];
        this.efficiencyCoeff = new double[variants];
        this.algorithmEfficiencyCoeff = new double[variants];
        this.criticalPath = new int[variants];
    }

    public void addData(int i, int totalExecTime, double accelarationCoeffI, double efficiencyCoeffI,
            double algorithmEfficiencyCoeffI, int criticalPathI) {
        totalExecutionTime[i] = totalExecTime;
        accelarationCoeff[i] = accelarationCoeffI;
        efficiencyCoeff[i] = efficiencyCoeffI;
        algorithmEfficiencyCoeff[i] = algorithmEfficiencyCoeffI;
        criticalPath[i] = criticalPathI;
    }

    public int getTotalExecutionTimeMedian() {
        Arrays.sort(totalExecutionTime);
        int totalExecutionTimeMedian = totalExecutionTime[variants / 2];
        return totalExecutionTimeMedian;
    }

    public double getAccelarationCoeffMedian() {
        Arrays.sort(accelarationCoeff);
        double accelarationCoeffMedian = accelarationCoeff[variants / 2];
        return accelarationCoeffMedian;
    }

    public double getEfficiencyCoeffMedian() {
        Arrays.sort(efficiencyCoeff);
        double efficiencyCoeffMedian = efficiencyCoeff[variants / 2];
        return efficiencyCoeffMedian;
    }

    public double getAlgorithmEfficiencyCoeffMedian() {
        Arrays.sort(algorithmEfficiencyCoeff);
        double algorithmEfficiencyCoeffMedian = algorithmEfficiencyCoeff[variants / 2];
        return algorithmEfficiencyCoeffMedian;
    }

    public int getCriticalPathMedian() {
        Arrays.sort(criticalPath);
        int criticalPathMedian = criticalPath[variants / 2];
        return criticalPathMedian;
    }
}
