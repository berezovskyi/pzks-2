package edu.kpi.pzks.console;

/**
 *
 * @author Aloren
 */
public class ConsoleData {

    public int numberOfNodes = 10;
    public int numberOfChannels = 2;
    public int minNodeWeight = 1;
    public int maxNodeWeight = 10;
    public String[] systemGraphs;
    public String pathToSaveGraph = "";
    public int[] queueVariant = {1};
    public int[] schedulerVariant = {6};
    public boolean isDuplex = true;
    public boolean isVerbose = false;
    public int variants = 100;
    public String picPath = "";
    public double conStart = 0.1;
    public double conStep = 0.1;
    public double conEnd = 1.0;
}
