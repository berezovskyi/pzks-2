package edu.kpi.pzks.console;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.geom.Ellipse2D;
import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.Map.Entry;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.labels.ItemLabelAnchor;
import org.jfree.chart.labels.ItemLabelPosition;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.TextAnchor;
import org.openide.util.Exceptions;

/**
 *
 * @author Aloren
 */
public class GraphConstructor {

    private final static Color linesColor = new Color(195, 195, 195);
    private final static Color backgroundColor = Color.white;

    public void save(Map<String, Map<Number, Number>> dataToSave, String fileName, String yName) {
        XYSeriesCollection xyDataset = new XYSeriesCollection();
        for (Entry<String, Map<Number, Number>> entry : dataToSave.entrySet()) {
            String queueScheduler = entry.getKey();
            Map<Number, Number> data = entry.getValue();
            XYSeries series = new XYSeries(queueScheduler);
            for (Entry<Number, Number> pair : data.entrySet()) {
                series.add(pair.getKey(), pair.getValue());
            }
            xyDataset.addSeries(series);
        }

        JFreeChart chart = ChartFactory.createXYLineChart("", "Cвязность", yName,
                xyDataset, PlotOrientation.VERTICAL, true, true, false);

        XYPlot plot = (XYPlot) chart.getPlot();

        plot.setBackgroundPaint(backgroundColor);
        plot.setDomainGridlinePaint(linesColor);
        plot.setDomainCrosshairPaint(linesColor);
        plot.setRangeGridlinePaint(linesColor);

        XYLineAndShapeRenderer renderer = (XYLineAndShapeRenderer) plot.getRenderer();
        for (int i = 0; i < xyDataset.getSeriesCount(); i++) {
            renderer.setSeriesShape(i, new Ellipse2D.Float(-3, -3, 6, 6));
            renderer.setSeriesShapesVisible(i, true);
            renderer.setSeriesShapesFilled(i, true);
            renderer.setSeriesStroke(i, new BasicStroke(2.f));
        }
        renderer.setBaseItemLabelsVisible(true);
        renderer.setBaseItemLabelPaint(Color.BLACK);
        renderer.setBasePositiveItemLabelPosition(new ItemLabelPosition(ItemLabelAnchor.INSIDE6, TextAnchor.CENTER));

        try {
            ChartUtilities.saveChartAsPNG(new File(fileName + ".png"), chart, 1000, 800);
        } catch (IOException ex) {
            Exceptions.printStackTrace(ex);
        }
    }
}
