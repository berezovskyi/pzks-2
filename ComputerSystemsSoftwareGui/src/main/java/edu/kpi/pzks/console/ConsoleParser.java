package edu.kpi.pzks.console;

import java.io.File;
import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

/**
 *
 * @author Aloren
 */
public class ConsoleParser {

    private final Options options = new Options();
    private final CommandLineParser parser = new BasicParser();
    private final HelpFormatter formatter = new HelpFormatter();
    private ConsoleData data = new ConsoleData();

    public ConsoleParser() {
        options.addOption("nn", "numbernodes", true, "number of nodes for task graph (required)");
        options.addOption("system", true, "path to the system graph .system file (required)");
        options.addOption("q", "queue", true, "variants of queue generator (1,4,8,9,12)(required)");
        options.addOption("s", "scheduler", true, "variants of scheduler (3,4,6)(required)");
        options.addOption("var", "variant", true, "number of generated graphs per connectivity");
        options.addOption("pic", "picpath", true, "path to the folder where to store pictures (must exist)");
        options.addOption("cstart", true, "connectivity start");
        options.addOption("cstep", true, "connectivity step");
        options.addOption("cend", true, "connectivity end");
        options.addOption("graph", true, "path where to store generated task graphs");
        options.addOption("nc", "numberchannels", true, "number of channels for system graph");
        options.addOption("min", "minweight", true, "min weight of the task node");
        options.addOption("max", "maxweight", true, "max weight of the task node");
        options.addOption("d", "duplex", false, "system graph links should be duplex");
        options.addOption("v", "verbose", true, "output data");
        options.addOption("h", "help", false, "output help");
    }

    public ConsoleData getData() {
        return data;
    }

    public boolean parseInputArgs(String[] args) throws ParseException {
        CommandLine cmd = null;
        try {
            cmd = parser.parse(options, args);
        } catch (ParseException e) {
            System.err.println("Parsing failed due to error: " + e.getMessage());
            outputHelp();
            return false;
        }
        if (args.length == 0) {
            outputHelp();
            return false;
        }
        if (cmd.hasOption("h")) {
            outputHelp();
            return false;
        }
        if (!cmd.hasOption("system") || !cmd.hasOption("nn") || !cmd.hasOption("s") || !cmd.hasOption("q")) {
            System.out.println("system, nn, s, q parameters are required!");
            outputHelp();
            return false;
        }
        if (cmd.hasOption("nn")) {
            data.numberOfNodes = Integer.parseInt(cmd.getOptionValue("nn"));
        }
        if (cmd.hasOption("nc")) {
            data.numberOfChannels = Integer.parseInt(cmd.getOptionValue("nc"));
        }
        if (cmd.hasOption("min")) {
            data.minNodeWeight = Integer.parseInt(cmd.getOptionValue("min"));
        }
        if (cmd.hasOption("max")) {
            data.maxNodeWeight = Integer.parseInt(cmd.getOptionValue("max"));
        }
        if (cmd.hasOption("var")) {
            data.variants = Integer.parseInt(cmd.getOptionValue("var"));
        }
        if (cmd.hasOption("cstart")) {
            data.conStart = Double.parseDouble(cmd.getOptionValue("cstart"));
        }
        if (cmd.hasOption("cstep")) {
            data.conStep = Double.parseDouble(cmd.getOptionValue("cstep"));
        }
        if (cmd.hasOption("cend")) {
            data.conEnd = Double.parseDouble(cmd.getOptionValue("cend"));
        }
        boolean validated = validateConnectionParameters();
        if (!validated) {
            return false;
        }
        if (cmd.hasOption("pic")) {
            data.picPath = cmd.getOptionValue("pic");
        }
        validated = validatePic();
        if (!validated) {
            return false;
        }
        if (cmd.hasOption("system")) {
            final String[] systemGraphs = cmd.getOptionValue("system").split(",");
            data.systemGraphs = systemGraphs;
        }
        if (cmd.hasOption("graph")) {
            data.pathToSaveGraph = cmd.getOptionValue("graph");
        }
        if (cmd.hasOption("q")) {
            String[] queues = cmd.getOptionValue("q").split(",");
            data.queueVariant = convertToIntArray(queues);
        }
        if (cmd.hasOption("s")) {
            String[] schedulers = cmd.getOptionValue("s").split(",");
            data.schedulerVariant = convertToIntArray(schedulers);
        }
        if (cmd.hasOption("d")) {
            data.isDuplex = true;
        }
        if (cmd.hasOption("v")) {
            final String optionValue = cmd.getOptionValue("v");
            data.isVerbose = Boolean.parseBoolean(optionValue);
        }
        return true;
    }

    private int[] convertToIntArray(String[] queueVarsStr) throws NumberFormatException {
        int[] queueVars = new int[queueVarsStr.length];
        for (int i = 0; i < queueVarsStr.length; i++) {
            queueVars[i] = Integer.parseInt(queueVarsStr[i]);
        }
        return queueVars;
    }

    private void outputHelp() {
        formatter.printHelp("java -jar pzks", options);
    }

    private boolean validateConnectionParameters() {
        if (data.conStart > data.conEnd) {
            System.err.println("cstart cannot be greater than cend.");
            return false;
        }
        if (data.conStep <= 0 || data.conStart <= 0 || data.conEnd <= 0) {
            System.out.println("cstep, cstart, cend can not be less or equal to 0.");
            return false;
        }
        return true;
    }

    private boolean validatePic() {
        File f = new File(data.picPath);
        if (f.exists() && !f.isDirectory()) {
            f.mkdir();
        } else if(!f.exists()){
            f.mkdir();
        }
        return true;

    }
}
