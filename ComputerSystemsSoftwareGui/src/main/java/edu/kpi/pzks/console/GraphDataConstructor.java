package edu.kpi.pzks.console;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 *
 * @author Aloren
 */
public class GraphDataConstructor {

    /**
     * Stores data in format: String: numberOfQueue-numberOfScheduler (for 1 and 2 variants it is "1-2") Map<Double,
     * Integer>: connectivity and totalTime
     */
    private Map<String, Map<Double, Integer>> totalTime = new LinkedHashMap<>();
    private Map<String, Map<Double, Double>> accelarationCoeff = new LinkedHashMap<>();
    private Map<String, Map<Double, Double>> efficiencyCoeff = new LinkedHashMap<>();
    private Map<String, Map<Double, Double>> algorithmEfficiencyCoeff = new LinkedHashMap<>();

    public GraphDataConstructor() {
    }

    public void addTotalTime(int queue, int scheduler, String systemName, double connectivity, int totalTime) {
        String queueScheduler = createKey(queue, scheduler, systemName);
        Map<Double, Integer> values = this.totalTime.get(queueScheduler);
        if (values == null) {
            values = new LinkedHashMap<>();
            this.totalTime.put(queueScheduler, values);
        }
        values.put(connectivity, totalTime);
    }

    public void addAccelarationCoeff(int queue, int scheduler, String systemName, double connectivity, double accelarationCoeff) {
        String queueScheduler = createKey(queue, scheduler, systemName);
        Map<Double, Double> values = this.accelarationCoeff.get(queueScheduler);
        if (values == null) {
            values = new LinkedHashMap<>();
            this.accelarationCoeff.put(queueScheduler, values);
        }
        values.put(connectivity, accelarationCoeff);
    }

    public void addEfficiencyCoeff(int queue, int scheduler, String systemName, double connectivity, double efficiencyCoeff) {
        String queueScheduler = createKey(queue, scheduler, systemName);
        Map<Double, Double> values = this.efficiencyCoeff.get(queueScheduler);
        if (values == null) {
            values = new LinkedHashMap<>();
            this.efficiencyCoeff.put(queueScheduler, values);
        }
        values.put(connectivity, efficiencyCoeff);
    }

    public void addAlgorithEfficiencyCoeff(int queue, int scheduler, String systemName, double connectivity, double algorithmEfficiencyCoeff) {
        String queueScheduler = createKey(queue, scheduler, systemName);
        Map<Double, Double> values = this.algorithmEfficiencyCoeff.get(queueScheduler);
        if (values == null) {
            values = new LinkedHashMap<>();
            this.algorithmEfficiencyCoeff.put(queueScheduler, values);
        }
        values.put(connectivity, algorithmEfficiencyCoeff);
    }

    public Map getTotalTime() {
        return totalTime;
    }

    public Map getAccelarationCoeff() {
        return accelarationCoeff;
    }

    public Map getEfficiencyCoeff() {
        return efficiencyCoeff;
    }

    public Map getAlgorithmEfficiencyCoeff() {
        return algorithmEfficiencyCoeff;
    }

    private String createKey(int queue, int scheduler, String systemName) {
        return new StringBuilder().append(queue).append("-").append(scheduler).append("-").append(systemName).toString();
    }
    
}
