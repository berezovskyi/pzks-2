package edu.kpi.pzks.console;

/**
 *
 * @author Aloren
 */
public class LatexTableConstructor {

    private StringBuilder sb = new StringBuilder();

    public LatexTableConstructor() {
    }
    
    /**
     * Cleans all data inside this constructor.
     */
    public void newTable() {
        this.sb = new StringBuilder();
    }

    public void appendSimulationResult(double connectivity, int totalExecutionTime,
            double accelarationCoeff, double efficiencyCoeff, double algorithmEfficiencyCoeff, int criticalPath) {
        appendFormattedLine("%3.1f & %d & %d & %5.3f & %5.3f & %5.3f \\\\", connectivity, totalExecutionTime, criticalPath,
                accelarationCoeff, efficiencyCoeff, algorithmEfficiencyCoeff);
        sb.append("\\hline\n");
    }

    public void appendFooter() {
        appendFormattedLine("\\end{longtable}");
    }

    public void appendHeader(int numberOfNodes, String topology) {
        sb.append("\\begin{longtable}{|>{\\centering\\arraybackslash}m{2"
                + ".6cm}|>{\\centering\\arraybackslash}m{2cm}|c|c|c|>{\\centering\\arraybackslash}m{1"
                + ".8cm}|>{\\centering\\arraybackslash}m{1.8cm}|}\n");
        sb.append(String.format("\\caption{Результаты исследования при количестве вершин, "
                + "равном %d, топологии %s \\label{table:characteristics}} \\\\\n", numberOfNodes, topology));
        sb.append("\\hline\n");

        appendFormattedLine("Связность & $T$ & $T_C$ & $K_S$ & $K_E$ & $E_{ALG}$\\\\");
        appendFormattedLine("\\hline");
        appendFormattedLine("\\endfirsthead");
        appendFormattedLine("\\multicolumn{6}{c}%%");
        appendFormattedLine("{\\tablename\\ \\thetable\\ -- \\textit{Продолжение}} \\\\");
        appendFormattedLine("\\hline");
        appendFormattedLine("Связность & $T$ & $T_C$ & $K_S$ & $K_E$ & $E_{ALG}$\\\\ %%  то же самое");
        appendFormattedLine("\\hline");
        appendFormattedLine("\\endhead");
        appendFormattedLine("\\endlastfoot");
        appendFormattedLine("");
    }

    private void appendFormattedLine(String format, Object... params) {
        sb.append(String.format(format, params));
        sb.append("\n");
    }
    
    /**
     * Constructs final table.
     * @return constructed table
     */
    public String construct() {
        return this.sb.toString();
    }

    public void appendMultiColumns(int schedulerVariant, int queueVariant) {
        appendFormattedLine("\\multicolumn{6}{|c|}{Алгоритм назначения: %s} \\\\*", schedulerVariant);
        appendFormattedLine("\\hline");
        appendFormattedLine("\\multicolumn{6}{|c|}{Алгоритм формирования очереди: %s} \\\\* ", queueVariant);
        appendFormattedLine("\\hline");
    }
}
