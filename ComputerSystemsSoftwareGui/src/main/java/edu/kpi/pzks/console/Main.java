package edu.kpi.pzks.console;

import edu.kpi.pzks.core.assign.ProcessorSystem;
import edu.kpi.pzks.core.assign.Scheduler;
import edu.kpi.pzks.core.assign.SimulationInfo;
import edu.kpi.pzks.core.exceptions.GraphException;
import edu.kpi.pzks.core.factory.GraphFactory;
import edu.kpi.pzks.core.model.Graph;
import edu.kpi.pzks.core.model.Node;
import edu.kpi.pzks.core.queue.Queue;
import edu.kpi.pzks.core.queue.factors.impl.*;
import edu.kpi.pzks.gui.io.GraphLoader;
import edu.kpi.pzks.gui.io.GraphSaver;
import edu.kpi.pzks.gui.io.impl.XmlGraphLoader;
import edu.kpi.pzks.gui.io.impl.XmlGraphSaver;
import edu.kpi.pzks.gui.layout.CircleLayout;
import edu.kpi.pzks.gui.modelview.GraphView;
import edu.kpi.pzks.gui.modelview.impl.GraphViewImpl;
import org.apache.commons.cli.*;

import java.awt.*;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import java.util.List;
import java.util.Map.Entry;

/**
 * @author Aloren
 */
public class Main {

    private final static GraphLoader loader = new XmlGraphLoader();
    private final static GraphSaver saver = new XmlGraphSaver();

    public static void main(String[] args) {
        try {
            GraphDataConstructor graphDataConstructor = new GraphDataConstructor();
            GraphConstructor graphConstructor = new GraphConstructor();
            ConsoleParser parser = new ConsoleParser();
            boolean parsed = parser.parseInputArgs(args);
            if (!parsed) {
                System.exit(0);
            }
            ConsoleData c = parser.getData();
            for (String systemGraphPath : c.systemGraphs) {
                final GraphView graphView = loader.loadFromFile(new File(systemGraphPath));
                String systemName = graphView.getName();
                Graph systemGraph = graphView.getGraph();
                outputStartData(c, systemGraph);
                Map<Double, List<Graph>> connectivityTaskGraphs = generateConnectivityTaskGraphs(c);
                LatexTableConstructor latexOutput = new LatexTableConstructor();
                latexOutput.appendHeader(c.numberOfNodes, systemName);
                for (int scheduler : c.schedulerVariant) {
                    for (int queue : c.queueVariant) {
                        latexOutput.appendMultiColumns(scheduler, queue);
                        for (Entry<Double, List<Graph>> entry : connectivityTaskGraphs.entrySet()) {
                            double connectivity = entry.getKey();
                            outputStepData(c.isVerbose, connectivity, scheduler, queue);

                            ParametersCounter counter = new ParametersCounter((c.variants));
                            final List<Graph> taskGraphs = entry.getValue();
                            for (int i = 0; i < taskGraphs.size(); i++) {
                                Graph taskGraph = taskGraphs.get(i);
                                int execTimeOnOneProcessorI = calculateTimeForOneProcessor(taskGraph);
                                int criticalPathI = getCriticalPath(taskGraph);
                                ProcessorSystem.Statistics statistics = getSimulationResults(scheduler, queue, c, taskGraph, systemGraph);
                                final double accelarationCoeffI = getAccelarationCoeff(execTimeOnOneProcessorI, statistics);
                                final double efficiencyCoeffI = getEfficiencyCoeff(accelarationCoeffI, systemGraph);
                                final double algorithmEfficiencyCoeffI = getAlgorithmEfficiencyCoeff(criticalPathI, statistics);
                                counter.addData(i, statistics.totalExecutionTime, accelarationCoeffI, efficiencyCoeffI, algorithmEfficiencyCoeffI, criticalPathI);
                            }
                            outputParameters(c.isVerbose, counter);

                            int totalExecutionTime = counter.getTotalExecutionTimeMedian();
                            double accelarationCoeff = counter.getAccelarationCoeffMedian();
                            double efficiencyCoeff = counter.getEfficiencyCoeffMedian();
                            double algorithmEfficiencyCoeff = counter.getAlgorithmEfficiencyCoeffMedian();
                            int criticalPath = counter.getCriticalPathMedian();

                            latexOutput.appendSimulationResult(connectivity, totalExecutionTime, accelarationCoeff,
                                    efficiencyCoeff, algorithmEfficiencyCoeff, criticalPath);

                            graphDataConstructor.addTotalTime(queue, scheduler, systemName, connectivity, totalExecutionTime);
                            graphDataConstructor.addEfficiencyCoeff(queue, scheduler, systemName, connectivity, efficiencyCoeff);
                            graphDataConstructor.addAlgorithEfficiencyCoeff(queue, scheduler, systemName, connectivity, algorithmEfficiencyCoeff);
                            graphDataConstructor.addAccelarationCoeff(queue, scheduler, systemName, connectivity, accelarationCoeff);
                        }
                    }
                }
                latexOutput.appendFooter();
                System.out.println(latexOutput.construct());
            }


            String name = createName(c);
            graphConstructor.save(graphDataConstructor.getAccelarationCoeff(), c.picPath + "\\acc-coef-" + name, "Коэффициент ускорения");
            graphConstructor.save(graphDataConstructor.getAlgorithmEfficiencyCoeff(), c.picPath + "\\alg-eff-coef-" + name, "Коэффициент эффективности алгоритма");
            graphConstructor.save(graphDataConstructor.getEfficiencyCoeff(), c.picPath + "\\eff-coef-" + name, "Коэффициент эффективности");
            graphConstructor.save(graphDataConstructor.getTotalTime(), c.picPath + "\\total-time-" + name, "Общее время выполнения");

        } catch (ParseException e) {
            System.err.println("Parsing failed due to error: " + e.getMessage());
        }
    }

    private static int calculateTimeForOneProcessor(Graph taskGraph) {
        int time = 0;
        for (Node node : taskGraph.getNodes()) {
            time += node.getWeight();
        }
        return time;
    }

    private static double getAccelarationCoeff(int execTimeOnOneProcessor, ProcessorSystem.Statistics statistics) {
        return (double) execTimeOnOneProcessor / statistics.totalExecutionTime;
    }

    private static double getEfficiencyCoeff(final double accelarationCoeff, Graph systemGraph) {
        return (double) accelarationCoeff / systemGraph.getNodes().size();
    }

    private static double getAlgorithmEfficiencyCoeff(int criticalPath, ProcessorSystem.Statistics statistics) {
        return (double) criticalPath / statistics.totalExecutionTime;
    }

//    private static void saveTaskGraph(Graph taskGraph, ConsoleData c, double connectivity) throws GraphException {
//        GraphView taskGraphView = GraphViewImpl.createView(taskGraph);
//        Dimension prefSize = new Dimension(500, 500);
//        taskGraphView.setBounds(new Rectangle(0, 0, (int) prefSize.getWidth(), (int) prefSize.getHeight()));
//        taskGraphView.layout(new CircleLayout());
//        saver.saveToFile(taskGraphView,
//                new File(c.pathToSaveGraph + "" + String.format(Locale.US, "%.1f", connectivity) + ".task"));
//    }
    private static int getCriticalPath(Graph taskGraph) {
        CriticalPathFactorEvaluator evaluator =
                new CriticalPathFactorEvaluator(taskGraph.getNodes(), taskGraph.getLinks());
        int criticalPath = evaluator.getCriticalPath();
        return criticalPath;
    }

    private static ProcessorSystem.Statistics getSimulationResults(int schedulerVariant, int queueVariant, ConsoleData c, Graph taskGraph, Graph systemGraph) {
        Queue queueGenerator = QueueFactory.getQueue(queueVariant, taskGraph.getNodes(), taskGraph.getLinks());
        Scheduler scheduler = SchedulerFactory.getScheduler(schedulerVariant);
        ProcessorSystem system = new ProcessorSystem(systemGraph, taskGraph);
        final SimulationInfo.LinkMode linkMode = c.isDuplex
                ? SimulationInfo.LinkMode.DUPLEX
                : SimulationInfo.LinkMode.HALF_DUPLEX;
        system.simulationInfo = new SimulationInfo(c.numberOfChannels, linkMode, queueGenerator);
        system.scheduler = scheduler;
        if (c.isVerbose) {
            System.out.println("Simulation started...");
        }
        system.simulation.start();
        ProcessorSystem.Statistics statistics = system.simulation.collectStatistics();
        return statistics;
    }

    private static void outputStartData(ConsoleData c, Graph systemGraph) {
        if (c.isVerbose) {
            System.out.println("System graph: \n" + systemGraph);
            System.out.println("Queue generators: " + Arrays.toString(c.queueVariant));
            System.out.println("Schedulers: " + Arrays.toString(c.schedulerVariant));
            System.out.println("Variants: " + c.variants);
        }
    }

    private static void outputStepData(boolean isVerbose, double connectivity, int scheduler, int queue) {
        if (isVerbose) {
            System.out.println("---------------------------------------------");
            System.out.println("Current connectivity: " + connectivity);
            System.out.println("Scheduler: " + scheduler + " queue: " + queue);
        }
    }

    private static void outputParameters(boolean isVerbose, ParametersCounter stat) {
        if (isVerbose) {
            System.out.println("Total execution time: " + Arrays.toString(stat.totalExecutionTime));
            System.out.println("Acceleration coeff: " + Arrays.toString(stat.accelarationCoeff));
            System.out.println("Efficiency coeff: " + Arrays.toString(stat.efficiencyCoeff));
            System.out.println("Algorithm efficiency coeff: " + Arrays.toString(stat.algorithmEfficiencyCoeff));
            System.out.println("Critical path: " + Arrays.toString(stat.criticalPath));
        }
    }

    private static Map<Double, List<Graph>> generateConnectivityTaskGraphs(ConsoleData c) {
        Map<Double, List<Graph>> connectivityTaskGraphs = new LinkedHashMap<>();
        for (double connectivity = c.conStart; connectivity <= c.conEnd; connectivity += c.conStep) {
            List<Graph> graphs = new ArrayList<>();
            for (int i = 0; i < c.variants; i++) {
                Graph taskGraph = GraphFactory.newGraph(c.numberOfNodes, c.minNodeWeight, c.maxNodeWeight, connectivity);
                graphs.add(taskGraph);
            }
            connectivityTaskGraphs.put(connectivity, graphs);
        }
        return connectivityTaskGraphs;
    }

    private static String createName(ConsoleData c) {
        StringBuilder name = new StringBuilder();
        name.append("q").append(Arrays.toString(c.queueVariant)).append("-");
        name.append("s").append(Arrays.toString(c.schedulerVariant)).append("-");
        name.append("nn").append(c.numberOfNodes).append("-");
        name.append("nc").append(c.numberOfChannels);
        return name.toString();
    }
}