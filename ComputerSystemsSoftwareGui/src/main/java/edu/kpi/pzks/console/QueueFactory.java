package edu.kpi.pzks.console;

import edu.kpi.pzks.core.model.Links;
import edu.kpi.pzks.core.model.Node;
import edu.kpi.pzks.core.queue.DoubleFactorQueue;
import edu.kpi.pzks.core.queue.Queue;
import edu.kpi.pzks.core.queue.SingleFactorQueue;
import edu.kpi.pzks.core.queue.factors.impl.ConnectivityDescFactorEvaluatorImpl;
import edu.kpi.pzks.core.queue.factors.impl.CriticalPathByNumberOfNodesAscFactorEvaluatorImpl;
import edu.kpi.pzks.core.queue.factors.impl.CriticalPathByNumberOfNodesDescFactorEvaluatorImpl;
import edu.kpi.pzks.core.queue.factors.impl.NodeWeightDescFactorEvaluator;
import edu.kpi.pzks.core.queue.factors.impl.OutLinksFactorEvaluatorImpl;
import edu.kpi.pzks.core.queue.factors.impl.WeightedCriticalFactorEvaluatorImpl;
import java.util.Set;

/**
 *
 * @author Aloren
 */
public class QueueFactory {

    public static Queue getQueue(int index, Set<Node> nodes, Links links) {
        Queue queueGenerator;
        switch (index) {
            case 1:
                queueGenerator = new SingleFactorQueue(new WeightedCriticalFactorEvaluatorImpl(nodes, links), nodes);
                break;

            case 4:
                queueGenerator = new DoubleFactorQueue(
                        new CriticalPathByNumberOfNodesDescFactorEvaluatorImpl(nodes, links),
                        new ConnectivityDescFactorEvaluatorImpl(nodes, links), nodes);
                break;
            case 8:
                queueGenerator = new DoubleFactorQueue(
                        new CriticalPathByNumberOfNodesAscFactorEvaluatorImpl(nodes, links),
                        new NodeWeightDescFactorEvaluator(), nodes);
                break;
            case 9:
                queueGenerator = new SingleFactorQueue(
                        new CriticalPathByNumberOfNodesAscFactorEvaluatorImpl(nodes, links), nodes);
                break;
            case 12:
                queueGenerator = new SingleFactorQueue(new OutLinksFactorEvaluatorImpl(), nodes);
                break;
            default:
                throw new IllegalArgumentException("Queue generator variant " + index + " is not implemented!");
        }
        return queueGenerator;
    }
}
