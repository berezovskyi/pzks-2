package edu.kpi.pzks.gui.ui.tools.impl;

import edu.kpi.pzks.gui.modelview.LinkView;
import edu.kpi.pzks.gui.modelview.NodeView;
import edu.kpi.pzks.gui.ui.panels.GraphPanel;
import edu.kpi.pzks.gui.ui.tools.AbstractTool;
import java.awt.event.MouseEvent;

/**
 * @author Aloren
 */
public class RemoveTool extends AbstractTool {

    private final GraphPanel graphPanel;

    public RemoveTool(GraphPanel graphPanel) {
        this.graphPanel = graphPanel;
    }

    @Override
    public void mouseClicked(MouseEvent me) {
        int x = me.getX();
        int y = me.getY();
        NodeView selectedNodeView = graphPanel.getGraphView().getNodeViewAtPoint(x, y);
        if (selectedNodeView != null) {
            graphPanel.getGraphView().removeNodeView(selectedNodeView);
            graphPanel.checkGraphIsValid();
            graphPanel.repaint();
        } else {
            LinkView selectedLinkView = graphPanel.getGraphView().getLinkViewAtPoint(x, y);
            if (selectedLinkView != null) {
                graphPanel.getGraphView().removeLinkView(selectedLinkView);
                graphPanel.checkGraphIsValid();
                graphPanel.repaint();
            }
        }
    }
}
