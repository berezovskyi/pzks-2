package edu.kpi.pzks.gui.actions.ui;

import edu.kpi.pzks.core.model.Graph;
import edu.kpi.pzks.gui.ui.MainFrame;
import edu.kpi.pzks.gui.ui.popups.SimulationSettingsDialog;

import javax.swing.*;
import java.awt.event.ActionEvent;

/**
 * @author smarx
 */
public class SimulateAction extends AbstractAction {

    private MainFrame mainFrame;

    public SimulateAction(MainFrame mainFrame) {
        this.mainFrame = mainFrame;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Graph taskGraph = mainFrame.getTaskPanel().getGraphView().getGraph();
        Graph systemGraph = mainFrame.getSystemPanel().getGraphView().getGraph();
        if (systemGraph.getNodes().isEmpty()) {
            JOptionPane.showMessageDialog(mainFrame, "There must be processors to start simulation.",
                    "Error", JOptionPane.ERROR_MESSAGE);
            return;
        }
        SimulationSettingsDialog simulationDialog = new SimulationSettingsDialog(mainFrame, taskGraph, systemGraph);
        simulationDialog.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        simulationDialog.pack();
        simulationDialog.setLocationRelativeTo(mainFrame);
        simulationDialog.setAlwaysOnTop(true);
        simulationDialog.setVisible(true);
    }
}
