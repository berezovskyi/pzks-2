package edu.kpi.pzks.gui.ui.popups;

import edu.kpi.pzks.gui.modelview.LinkView;
import edu.kpi.pzks.gui.utils.STRINGS;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JMenuItem;

/**
 * @author aloren
 */
public class LinkViewPopup extends GraphPopup {

    public LinkViewPopup(LinkView linkView) {
        super(linkView);
    }

    @Override
    protected List<JMenuItem> getFields() {
        List<JMenuItem> items = new ArrayList<>(2);
        items.add(getWeightField());
        if (((LinkView) view).hasBendPoint()) {
            items.add(getRemoveBendPointField());
        }
        return items;
    }

    private JMenuItem getRemoveBendPointField() {
        JMenuItem removeBendPointItem = new JMenuItem(STRINGS.REMOVE_BEND_POINT);
        removeBendPointItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ((LinkView)view).removeBendPoint();
                getInvoker().repaint();
            }
        });
        return removeBendPointItem;
    }
}
