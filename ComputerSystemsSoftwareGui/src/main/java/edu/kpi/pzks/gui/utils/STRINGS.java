package edu.kpi.pzks.gui.utils;

import java.util.ResourceBundle;

/**
 * @author aloren
 */
public class STRINGS {

    private static final ResourceBundle strings = Resources.strings;
    private static final ResourceBundle menu = Resources.menu;
    public static final String MAIN_TITLE = strings.getString("main.title");
    public static final String ERROR_VALIDATION_TITLE = strings.getString("error.validation.title");
    public static final String WEIGHT = strings.getString("weight");
    public static final String REMOVE_BEND_POINT = strings.getString("removeBendPoint");
    public static final String CHANGE_WEIGHT = strings.getString("changeWeight");
    public static final String REMOVE = strings.getString("remove");
    public static final String SELECT = strings.getString("select");
    public static final String MODELING_MENU = menu.getString("modelingMenu");
    public static final String STATISTIC_MENU = menu.getString("statisticMenu");
    public static final String HELP_MENU = menu.getString("helpMenu");
    public static final String FILE_MENU = menu.getString("fileMenu");
    public static final String OPEN = menu.getString("open");
    public static final String SAVE = menu.getString("save");
    public static final String EXIT = menu.getString("exit");
    public static final String NEW = menu.getString("new");

    public static final String OKAY = menu.getString("okay");
    public static final String CANCEL = menu.getString("cancel");
    public static final String START = menu.getString("start");
    public static final String GEN_TASK_GRAPH = menu.getString("generateTaskGraph");
    public static final String GEN_SYSTEM_GRAPH = menu.getString("generateSystemGraph");
    public static final String GEN_GRAPH = menu.getString("generateGraph");
    public static final String TASK_GRAPH = menu.getString("taskGraph");
    public static final String SYSTEM_GRAPH = menu.getString("systemGraph");
    public static final String MODELING = menu.getString("modeling");
    public static final String MODELING_SETTINGS = menu.getString("modelingSettings");
    
    public static final String NUM_NODES = strings.getString("numberOfNodes");
    public static final String NODE_WEIGHT = strings.getString("nodeWeight");
    public static final String MIN_WEIGHT = strings.getString("minWeight");
    public static final String MAX_WEIGHT = strings.getString("maxWeight");
    public static final String CONNECTIVITY = strings.getString("connectivity");
    
    public static final String ALGO = strings.getString("algo");
    
    public static final String PARAMS = strings.getString("params");
    public static String FORM_QUEUE = strings.getString("formQueue");
    public static String FORM_PLAN = strings.getString("formPlan");
    
    public static String NODE = strings.getString("node");
    public static String LINK = strings.getString("link");
    public static String QUEUE = strings.getString("queue");
    public static String SCHEDULER = strings.getString("scheduler");
    public static String NUM_CHANNELS = strings.getString("numberOfChannels");
    public static String CHANNELS_TYPE = strings.getString("channelType");
    public static String DUPLEX = strings.getString("duplex");
    
    public static final String TOTAL_TIME = strings.getString("totalTime");
    public static final String PROCESSORS = strings.getString("processors");
    public static final String TIME = strings.getString("time");
    

}
