package edu.kpi.pzks.gui.ui.panels;

import edu.kpi.pzks.gui.ui.MainFrame;
import edu.kpi.pzks.gui.utils.CONSTANTS;
import edu.kpi.pzks.gui.utils.STRINGS;
import java.awt.Dimension;
import java.awt.GridLayout;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 *
 * @author aloren
 */
public class GraphGeneratorPanel extends JPanel {

    private final MainFrame mainFrame;
    private final JSpinner numberOfNodesSpinner;
    private final JSpinner minWeightSpinner;
    private final JSpinner maxWeightSpinner;
    private final JSpinner connectivitySpinner;
    
    private final static double MAX_CONNECTIVIY = 1.0;
    private final static double MIN_CONNECTIVITY = 0.1;
    private final static double DEF_CONNECTIVITY = 0.5;
    private final static double CONNECTIVITY_STEP = 0.1;
    
    private final static int DEF_NUM_NODES = 4;
    private final static int MIN_NUM_NODES = 2;
    private final static int MAX_NUM_NODES = 50;
    
    private final static int DEF_STEP = 1;
    private final static int DEF_MIN_WEIGHT = 1;
    private final static int MAX_WEIGHT = 100;
    private final static int MIN_WEIGHT = 1;
    private final static int DEF_MAX_WEIGHT = 10;

    public GraphGeneratorPanel(MainFrame mainFrame) {
        this.mainFrame = mainFrame;
        JLabel numberOfNodesLabel = new JLabel(STRINGS.NUM_NODES, JLabel.RIGHT);
        JLabel minWeightLabel = new JLabel(STRINGS.MIN_WEIGHT, JLabel.RIGHT);
        JLabel maxWeightLabel = new JLabel(STRINGS.MAX_WEIGHT, JLabel.RIGHT);
        JLabel connectivityLabel = new JLabel(STRINGS.CONNECTIVITY, JLabel.RIGHT);


        SpinnerNumberModel numberOfNodesSpinnerModel = new SpinnerNumberModel(DEF_NUM_NODES, MIN_NUM_NODES, MAX_NUM_NODES, DEF_STEP);
        this.numberOfNodesSpinner = new JSpinner(numberOfNodesSpinnerModel);


        final SpinnerNumberModel minWeightSpinnerModel = new SpinnerNumberModel(DEF_MIN_WEIGHT, MIN_WEIGHT, DEF_MAX_WEIGHT, DEF_STEP);
        this.minWeightSpinner = new JSpinner(minWeightSpinnerModel);

        
        SpinnerNumberModel maxWeightSpinnerModel = new SpinnerNumberModel(DEF_MAX_WEIGHT, MIN_WEIGHT, MAX_WEIGHT, DEF_STEP);
        this.maxWeightSpinner = new JSpinner(maxWeightSpinnerModel);
        maxWeightSpinner.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                final int curMaxValue = (Integer) maxWeightSpinner.getValue();
                final int curMinValue = (Integer) minWeightSpinner.getValue();
                if (curMinValue > curMaxValue) {
                    minWeightSpinner.setValue(curMaxValue);
                }
                minWeightSpinnerModel.setMaximum(curMaxValue);
            }
        });

        SpinnerNumberModel connectivitySpinnerModel = new SpinnerNumberModel(DEF_CONNECTIVITY, MIN_CONNECTIVITY, MAX_CONNECTIVIY, CONNECTIVITY_STEP);
        this.connectivitySpinner = new JSpinner(connectivitySpinnerModel);

        setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));

        JPanel paramsPanel = new JPanel(new GridLayout(2, 2, 10, 10));
        paramsPanel.setBorder(BorderFactory.createTitledBorder(STRINGS.PARAMS));
        paramsPanel.add(numberOfNodesLabel);
        paramsPanel.add(numberOfNodesSpinner);
        paramsPanel.add(connectivityLabel);
        paramsPanel.add(connectivitySpinner);
        add(paramsPanel);
        add(Box.createRigidArea(new Dimension(200, 10)));

        JPanel weightPanel = new JPanel();
        weightPanel.setBorder(BorderFactory.createTitledBorder(STRINGS.NODE_WEIGHT));
        weightPanel.setLayout(new GridLayout(2, 2, 10, 10));
        weightPanel.add(minWeightLabel);
        weightPanel.add(minWeightSpinner);
        weightPanel.add(maxWeightLabel);
        weightPanel.add(maxWeightSpinner);
        add(weightPanel);
        add(Box.createRigidArea(new Dimension(200, 10)));
    }

    public int getNumberOfNodes() {
        return (Integer) numberOfNodesSpinner.getValue();
    }

    public int getMinNodeWeight() {
        return (Integer) minWeightSpinner.getValue();
    }

    public int getMaxNodeWeight() {
        return (Integer) maxWeightSpinner.getValue();
    }

    public double getConnectivity() {
        return (Double) connectivitySpinner.getValue();
    }
}
