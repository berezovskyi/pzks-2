package edu.kpi.pzks.gui.ui.tools.impl;

import edu.kpi.pzks.gui.modelview.NodeView;
import edu.kpi.pzks.gui.ui.panels.GraphPanel;
import edu.kpi.pzks.gui.ui.popups.ChangeWeightDialog;
import edu.kpi.pzks.gui.ui.tools.AbstractTool;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import javax.swing.JPopupMenu;

/**
 * @author Aloren
 */
public class NodeSelectionTool extends AbstractTool {

    protected final GraphPanel graphPanel;

    public NodeSelectionTool(GraphPanel graphPanel) {
        this.graphPanel = graphPanel;
    }

    @Override
    public void paint(Graphics2D g2) {
        for (NodeView selectedNodeView : graphPanel.getSelectedNodeViews()) {
            selectedNodeView.paint(g2);
        }
    }

    @Override
    public void mouseClicked(MouseEvent me) {
        int x = me.getX();
        int y = me.getY();
        NodeView nodeAtPoint = graphPanel.getGraphView().getNodeViewAtPoint(x, y);
        if (nodeAtPoint != null) {
            graphPanel.clearSelectedNodeViews();
            graphPanel.addSelectedNodeView(nodeAtPoint);
            if (me.getModifiers() == MouseEvent.BUTTON3_MASK) {
                JPopupMenu pp = nodeAtPoint.getPopupMenu();
                pp.show(me.getComponent(), me.getX(), me.getY());
            }
            if (me.getClickCount() == 2) {
                ChangeWeightDialog dialog = new ChangeWeightDialog(nodeAtPoint);
                dialog.setLocationRelativeTo(null);
                dialog.setModal(true);
                dialog.setVisible(true);
            }
        } else {
            graphPanel.clearSelectedNodeViews();
        }
        graphPanel.repaint();
    }

    @Override
    public void destroy() {
        graphPanel.clearSelectedNodeViews();
    }
}
