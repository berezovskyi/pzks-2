package edu.kpi.pzks.gui.modelview.impl;

import edu.kpi.pzks.core.model.Link;
import edu.kpi.pzks.gui.modelview.LinkView;
import edu.kpi.pzks.gui.modelview.NodeView;
import edu.kpi.pzks.gui.ui.popups.LinkViewPopup;
import edu.kpi.pzks.gui.utils.COLORS;
import edu.kpi.pzks.gui.utils.CONSTANTS;
import edu.kpi.pzks.gui.utils.PaintUtils;
import java.awt.BasicStroke;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import javax.swing.JPopupMenu;

/**
 * @author aloren
 */
public class LinkViewImpl implements LinkView {

    private static final int HIT_BOX_SIZE = 6;
    protected NodeView fromNodeView;
    protected NodeView toNodeView;
    // If no bendPoint then it is (-1;-1)
    protected Point bendPoint;
    protected Link link;
    protected boolean isSelected;
    private boolean isOriented;

    public LinkViewImpl(Link link) {
        this(null, null, link);
    }

    public LinkViewImpl(NodeView fromNodeView, NodeView toNodeView, Link link) {
        this.link = link;
        this.fromNodeView = fromNodeView;
        this.toNodeView = toNodeView;
        this.bendPoint = new Point(-1, -1);
    }

    @Override
    public Link getLink() {
        return this.link;
    }

    @Override
    public void setBendPoint(int x, int y) {
        this.bendPoint = new Point(x, y);
    }

    @Override
    public Point getBendPoint() {
        return bendPoint;
    }
    
        @Override
    public void removeBendPoint() {
        this.bendPoint = new Point(-1, -1);
    }

    @Override
    public NodeView getFromNodeView() {
        return fromNodeView;
    }

    @Override
    public void setFromNodeView(NodeView fromNodeView) {
        this.fromNodeView = fromNodeView;
    }

    @Override
    public NodeView getToNodeView() {
        return toNodeView;
    }

    @Override
    public void setToNodeView(NodeView toNodeView) {
        this.toNodeView = toNodeView;
    }

    @Override
    public void paint(Graphics2D g2) {
        if (isSelected) {
            g2.setStroke(new BasicStroke(3));
            g2.setColor(COLORS.LINK_SELECTED_COLOR);
        } else {
            g2.setColor(COLORS.LINK_COLOR);
        }
        final int srcX = (int) (fromNodeView.getCenter().getX());
        final int srcY = (int) (fromNodeView.getCenter().getY());

        final int destX = (int) (toNodeView.getCenter().getX());
        final int destY = (int) (toNodeView.getCenter().getY());

        g2.setFont(PaintUtils.getFont());

        if (!hasBendPoint()) {
            drawWeightWithoutBendPoint(g2, srcX, destX, srcY, destY);
            drawLine(g2, srcX, srcY, destX, destY);
            double aDir = Math.atan2(srcX - destX, srcY - destY);
            if (isOriented) {
                drawArrowHead(g2, aDir, srcX, srcY, destX, destY);
            }
        } else {
            drawWeightWithBendPoint(g2);
            drawLineWithBendPoint(srcX, srcY, destX, destY, g2);
            double aDir = Math.atan2(bendPoint.getX() - destX, bendPoint.getY() - destY);
            if (isOriented) {
                drawArrowHead(g2, aDir, (int) bendPoint.getX(), (int) bendPoint.getY(), destX, destY);
            }
        }
        g2.setStroke(new BasicStroke(1));
    }

    @Override
    public boolean isSelected() {
        return isSelected;
    }

    @Override
    public void setSelected(boolean selected) {
        this.isSelected = selected;
    }

    /**
     * Draws arrow-head of the link.
     */
    private void drawArrowHead(Graphics2D g2, double aDir, int srcX, int srcY, int destX, int destY) {
        double dx = PaintUtils.getLength(srcX, destX);
        double dy = PaintUtils.getLength(srcY, destY);
        double theta = Math.atan2(dy, dx);
        double reverseTheta = (theta > 0.0d) ? (theta - Math.PI) : (theta + Math.PI);
        Point2D.Double destIntersectionP = PaintUtils.getEllipseIntersectionPoint(reverseTheta,
                toNodeView.getWidth(), toNodeView.getHeight());
        int x = (int) (destX + destIntersectionP.getX());
        int y = (int) (destY + destIntersectionP.getY());

        g2.setStroke(new BasicStroke(1f));
        Polygon arrowHead = new Polygon();
        float stroke = 0.2f;
        int i1 = 12 + (int) (stroke * 2);
        int i2 = 6 + (int) stroke;

        arrowHead.addPoint(x, y);// arrow tip
        arrowHead.addPoint(x + PaintUtils.xCor(i1, aDir + 0.5), y + PaintUtils.yCor(i1, aDir + 0.5));
        arrowHead.addPoint(x + PaintUtils.xCor(i2, aDir), y + PaintUtils.yCor(i2, aDir));
        arrowHead.addPoint(x + PaintUtils.xCor(i1, aDir - 0.5), y + PaintUtils.yCor(i1, aDir - 0.5));
        arrowHead.addPoint(x, y);// arrow tip
        g2.drawPolygon(arrowHead);
        g2.fillPolygon(arrowHead);

    }

    @Override
    public JPopupMenu getPopupMenu() {
        return new LinkViewPopup(this);
    }

    @Override
    public boolean contains(int x, int y) {
        int boxX = x - HIT_BOX_SIZE / 2;
        int boxY = y - HIT_BOX_SIZE / 2;
        int width = HIT_BOX_SIZE;
        int height = HIT_BOX_SIZE;
        final Line2D.Double[] lines = getLines();
        for (int j = 0; j < lines.length; j++) {
            Line2D.Double line = lines[j];
            if (line.intersects(boxX, boxY, width, height)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Line2D.Double[] getLines() {
        Point fromCenter = fromNodeView.getCenter();
        Point toCenter = toNodeView.getCenter();
        if (hasBendPoint()) {
            return new Line2D.Double[]{new Line2D.Double(toCenter, bendPoint),
                new Line2D.Double(bendPoint, fromCenter)};
        } else {
            return new Line2D.Double[]{new Line2D.Double(toCenter, fromCenter)};
        }
    }

    @Override
    public boolean hasBendPoint() {
        return !(bendPoint.x == -1 && bendPoint.y == -1);
    }

    @Override
    public void setLink(Link link) {
        this.link = link;
    }

    private void drawWeightWithoutBendPoint(Graphics2D g2, final int srcX, final int destX, final int srcY, final int destY) {
        g2.drawString(Double.toString(link.getWeight()), (srcX + destX) / 2 + 10, (srcY + destY) / 2 + 10);
    }

    private void drawWeightWithBendPoint(Graphics2D g2) {
        g2.drawString(Double.toString(link.getWeight()), (int) bendPoint.getX() + 10, (int) bendPoint.getY() + 10);
    }

    @Override
    public LinkViewPopup getPopup() {
        return new LinkViewPopup(this);
    }

    @Override
    public void setWeight(int weight) {
        this.link.setWeight(weight);
    }

    @Override
    public int getWeight() {
        return link.getWeight();
    }

    @Override
    public void setOriented(boolean isOriented) {
        this.isOriented = isOriented;
    }

    private void drawLineWithBendPoint(double srcX, double srcY, double destX, double destY, Graphics g) {
        double theta = getTheta(srcX, srcY, bendPoint.getX(), bendPoint.getY());
        Point2D.Double srcIntersectionP = getEllipseIntersectionPoint(theta, CONSTANTS.NODE_WIDTH, CONSTANTS.NODE_HEIGHT);
        g.drawLine((int) (srcX + srcIntersectionP.getX()), (int) (srcY + srcIntersectionP.getY()),
                (int) bendPoint.getX(), (int) bendPoint.getY());

        theta = getTheta(bendPoint.getX(), bendPoint.getY(), destX, destY);
        double reverseTheta = (theta > 0.0d) ? theta - Math.PI : theta + Math.PI;
        Point2D.Double destIntersectionP = getEllipseIntersectionPoint(reverseTheta, CONSTANTS.NODE_WIDTH, CONSTANTS.NODE_HEIGHT);
        g.drawLine((int) bendPoint.getX(), (int) bendPoint.getY(),
                (int) (destX + destIntersectionP.getX()), (int) (destY + destIntersectionP.getY()));
    }

    protected void drawLine(Graphics2D g2, final int srcX, final int srcY, final int destX, final int destY) {
        double dx = getLength(srcX, destX);
        double dy = getLength(srcY, destY);
        double theta = Math.atan2(dy, dx);
        Point2D.Double srcIntersectionP = getEllipseIntersectionPoint(theta, CONSTANTS.NODE_WIDTH, CONSTANTS.NODE_HEIGHT);
        Point2D.Double destIntersectionP = getEllipseIntersectionPoint(theta, CONSTANTS.NODE_WIDTH, CONSTANTS.NODE_HEIGHT);
        g2.drawLine((int) (srcX + srcIntersectionP.getX()), (int) (srcY + srcIntersectionP.getY()),
                (int) (destX - destIntersectionP.getX()), (int) (destY - destIntersectionP.getY()));
    }

    /**
     * Returns the length of a line ensuring it is not too small to render.
     *
     * @param start
     * @param end
     * @return
     */
    private double getLength(double start, double end) {
        double length = end - start;
        // make sure dx is not zero or too small
        if (length < 0.01 && length > -0.01) {
            if (length > 0) {
                length = 0.01;
            } else if (length < 0) {
                length = -0.01;
            }
        }
        return length;
    }

    /**
     * Finds the intersection point between link-line and the input Petri
     * node(Node) edge.
     *
     * @param theta
     * @param ellipseWidth
     * @param ellipseHeight
     * @return
     */
    private static Point2D.Double getEllipseIntersectionPoint(double theta, double ellipseWidth, double ellipseHeight) {
        double nhalfw = ellipseWidth / 2.0; // half elllipse width
        double nhalfh = ellipseHeight / 2.0; // half ellipse height
        double tanTheta = Math.tan(theta);

        double a = nhalfw;
        double b = nhalfh;
        double x = (a * b) / Math.sqrt(Math.pow(b, 2) + Math.pow(a, 2) * Math.pow(tanTheta, 2));
        if ((theta > Math.PI / 2.0 && theta < 1.5 * Math.PI) || (theta < -Math.PI / 2.0 && theta > -1.5 * Math.PI)) {
            x = -x;
        }
        double y = tanTheta * x;
        Point2D.Double p = new Point2D.Double(x, y);
        return p;
    }

    protected double getTheta(double srcX, double srcY, double dstX, double dstY) {
        double dx = getLength(srcX, dstX);
        double dy = getLength(srcY, dstY);
        double theta = Math.atan2(dy, dx);
        return theta;
    }
}
