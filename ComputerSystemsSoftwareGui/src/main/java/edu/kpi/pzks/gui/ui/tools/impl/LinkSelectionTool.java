package edu.kpi.pzks.gui.ui.tools.impl;

import edu.kpi.pzks.gui.modelview.LinkView;
import edu.kpi.pzks.gui.ui.panels.GraphPanel;
import edu.kpi.pzks.gui.ui.tools.AbstractTool;
import java.awt.Graphics2D;

import java.awt.event.MouseEvent;
import javax.swing.JPopupMenu;

/**
 * @author Aloren
 */
public class LinkSelectionTool extends AbstractTool {

    private final GraphPanel graphPanel;

    public LinkSelectionTool(GraphPanel graphPanel) {
        this.graphPanel = graphPanel;
    }

    @Override
    public void paint(Graphics2D g2) {
        for (LinkView selectedLinkView : graphPanel.getSelectedLinkViews()) {
            selectedLinkView.paint(g2);
        }
    }

    @Override
    public void mouseClicked(MouseEvent me) {
        int x = me.getX();
        int y = me.getY();
        LinkView selectedLinkView = graphPanel.getGraphView().getLinkViewAtPoint(x, y);
        if (selectedLinkView != null) {
            setSelectedLinkView(selectedLinkView);
            if (me.getModifiers() == MouseEvent.BUTTON3_MASK) {
                JPopupMenu pp = selectedLinkView.getPopupMenu();
                pp.show(me.getComponent(), me.getX(), me.getY());
            }
        } else {
            graphPanel.clearSelectedLinkViews();
        }
        graphPanel.repaint();
    }

    @Override
    public void mousePressed(MouseEvent me) {
        int x = me.getX();
        int y = me.getY();
        LinkView linkViewAtPoint = graphPanel.getGraphView().getLinkViewAtPoint(x, y);
        if (!isLinkSelected(linkViewAtPoint)) {
            setSelectedLinkView(linkViewAtPoint);
        }
        graphPanel.repaint();
    }

    private void setSelectedLinkView(LinkView selectedLinkView) {
        graphPanel.clearSelectedLinkViews();
        graphPanel.addSelectedLinkView(selectedLinkView);
    }

    private boolean isLinkSelected(LinkView linkViewAtPoint) {
        return !(linkViewAtPoint != null
                && !graphPanel.getSelectedLinkViews().contains(linkViewAtPoint));
    }

    @Override
    public void destroy() {
        graphPanel.clearSelectedLinkViews();
    }
}
