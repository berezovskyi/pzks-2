package edu.kpi.pzks.gui.ui.popups;

import edu.kpi.pzks.gui.modelview.NodeView;
import java.util.Arrays;
import java.util.List;
import javax.swing.JMenuItem;

/**
 * @author aloren
 */
public class NodeViewPopup extends GraphPopup {

    public NodeViewPopup(NodeView nodeView) {
        super(nodeView);
    }

    @Override
    protected List<JMenuItem> getFields() {
        return Arrays.asList(getWeightField());
    }
}
