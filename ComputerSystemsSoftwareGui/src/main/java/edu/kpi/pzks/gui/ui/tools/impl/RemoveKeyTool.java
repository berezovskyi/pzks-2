package edu.kpi.pzks.gui.ui.tools.impl;

import edu.kpi.pzks.gui.ui.panels.GraphPanel;
import edu.kpi.pzks.gui.ui.tools.AbstractTool;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;

/**
 * @author aloren
 */
public class RemoveKeyTool extends AbstractTool implements KeyListener {

    private final GraphPanel graphPanel;

    public RemoveKeyTool(GraphPanel graphPanel) {
        this.graphPanel = graphPanel;
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        this.graphPanel.requestFocusInWindow();
    }

    @Override
    public void mousePressed(MouseEvent e) {
        this.graphPanel.requestFocusInWindow();
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
    }

    @Override
    public void keyReleased(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_DELETE) {
            graphPanel.removeSelectedLinkViews();
            graphPanel.removeSelectedNodeViews();
            graphPanel.checkGraphIsValid();
            graphPanel.checkSize();
            graphPanel.repaint();
        }
    }
}
