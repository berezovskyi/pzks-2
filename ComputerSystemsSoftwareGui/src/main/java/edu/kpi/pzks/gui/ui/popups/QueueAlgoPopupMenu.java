package edu.kpi.pzks.gui.ui.popups;

import edu.kpi.pzks.core.model.Graph;
import edu.kpi.pzks.core.model.Link;
import edu.kpi.pzks.core.model.Node;
import edu.kpi.pzks.core.queue.DoubleFactorQueue;
import edu.kpi.pzks.core.queue.Queue;
import edu.kpi.pzks.core.queue.QueuedNode;
import edu.kpi.pzks.core.queue.SingleFactorQueue;
import edu.kpi.pzks.core.queue.factors.impl.CriticalPathByNumberOfNodesAscFactorEvaluatorImpl;
import edu.kpi.pzks.core.queue.factors.impl.CriticalPathByTimeForAllNodesDescFactorEvaluatorImpl;
import edu.kpi.pzks.core.queue.factors.impl.NodeWeightDescFactorEvaluator;
import edu.kpi.pzks.core.queue.factors.impl.OutLinksFactorEvaluatorImpl;
import edu.kpi.pzks.core.queue.factors.impl.WeightedCriticalFactorEvaluatorImpl;
import edu.kpi.pzks.gui.ui.MainFrame;
import edu.kpi.pzks.gui.ui.panels.GraphPanel;
import edu.kpi.pzks.gui.utils.STRINGS;
import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collection;
import java.util.Set;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;

/**
 *
 * @author Aloren
 */
public class QueueAlgoPopupMenu extends JPopupMenu {
    
    private final String algo1 = STRINGS.ALGO + " 1";
    private final String algo3 = STRINGS.ALGO + " 3";
    private final String algo8 = STRINGS.ALGO + " 8";
    private final String algo12 = STRINGS.ALGO + " 12";

    public QueueAlgoPopupMenu(final MainFrame mainFrame) {
        final GraphPanel taskPanel = mainFrame.getTaskPanel();
        
        final JMenuItem algo1Item = new JMenuItem(algo12);//12 variant
        algo1Item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                Set<Node> nodes = taskPanel.getGraphView().getGraph().getNodes();
                if (isNodesEmpty(nodes)) {
                    return;
                }
                final String algoName = algo1Item.getText();
                Queue queueGenerator = new SingleFactorQueue(new OutLinksFactorEvaluatorImpl(), nodes);
                outputToFrame(queueGenerator, algoName, mainFrame);
            }
        });
        
        final JMenuItem algo2Item = new JMenuItem(algo8);//8 variant
        algo2Item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                final Graph graph = taskPanel.getGraphView().getGraph();
                Set<Node> nodes = graph.getNodes();
                Set<Link> links = graph.getLinks();
                if (isNodesEmpty(nodes)) {
                    return;
                }
                final String algoName = algo2Item.getText();
                Queue queueGenerator = new DoubleFactorQueue(new CriticalPathByNumberOfNodesAscFactorEvaluatorImpl(nodes, links),
                        new NodeWeightDescFactorEvaluator(),
                        nodes);
                outputToFrame(queueGenerator, algoName, mainFrame);
            }
        });
        
        final JMenuItem algo3Item = new JMenuItem(algo1);//1 variant
        algo3Item.addActionListener(new MyActionListener(mainFrame, algo3Item));
        
        final JMenuItem algo4Item = new JMenuItem(algo3);//1 variant
        algo4Item.addActionListener(new MyActionListener3(mainFrame, algo4Item));
        add(algo1Item);
        add(algo2Item);
        add(algo3Item);
        add(algo4Item);
    }

    private static class MyActionListener implements ActionListener {

        protected final GraphPanel taskPanel;
        protected final MainFrame mainFrame;
        protected final JMenuItem item;

        public MyActionListener(MainFrame mainFrame, JMenuItem item) {
            this.taskPanel = mainFrame.getTaskPanel();
            this.mainFrame = mainFrame;
            this.item = item;
        }

        @Override
        public void actionPerformed(ActionEvent ae) {
            final Graph graph = taskPanel.getGraphView().getGraph();
            Set<Node> nodes = graph.getNodes();
            Set<Link> links = graph.getLinks();
            if (isNodesEmpty(nodes)) {
                return;
            }
            final String algoName = item.getText();
            Queue queueGenerator = new SingleFactorQueue(new WeightedCriticalFactorEvaluatorImpl(nodes, links), nodes);
            outputToFrame(queueGenerator, algoName, mainFrame);
        }
    }

    private static class MyActionListener3 extends MyActionListener {

        public MyActionListener3(MainFrame mainFrame, JMenuItem item) {
           super(mainFrame, item);
        }

        @Override
        public void actionPerformed(ActionEvent ae) {
            final Graph graph = taskPanel.getGraphView().getGraph();
            Set<Node> nodes = graph.getNodes();
            Set<Link> links = graph.getLinks();
            if (isNodesEmpty(nodes)) {
                return;
            }
            final String algoName = item.getText();
            Queue queueGenerator = new SingleFactorQueue(new CriticalPathByTimeForAllNodesDescFactorEvaluatorImpl(nodes, links), nodes);
            outputToFrame(queueGenerator, algoName, mainFrame);
        }
    }

    protected static void outputToFrame(Queue queueGenerator, final String algoName, MainFrame mainFrame) {
        Collection<QueuedNode> queuedNodes = queueGenerator.evaluate();
        String queueString = convertQueueToString(queuedNodes, algoName);
        JFrame outputFrame = getOutputFrame(algoName, queueString, mainFrame);
        outputFrame.setVisible(true);
    }

    private static String convertQueueToString(Collection<QueuedNode> queuedNodes, String algoName) {
        StringBuilder queue = new StringBuilder("<h2>" + algoName + STRINGS.QUEUE+" </h2>\r\n");
        queue.append("<dl>");
        for (QueuedNode queuedNode : queuedNodes) {
            queue.append("<dt>").append(queuedNode).append("</dt>").append("\r\n");
        }
        queue.append("</dl>");
        return queue.toString();
    }

    private static boolean isNodesEmpty(Set<Node> nodes) throws HeadlessException {
        if (nodes.isEmpty()) {
            JOptionPane.showMessageDialog(null, "Graph is empty. Can't produce queue", "Warning", JOptionPane.WARNING_MESSAGE);
            return true;
        }
        return false;
    }

    private static JFrame getOutputFrame(final String algoName, final String queueString, MainFrame mainFrame) {
        JFrame outputFrame = new JFrame(algoName);
        outputFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        outputFrame.setLayout(new BorderLayout());
        JEditorPane queueOutputPane = new JEditorPane();
        queueOutputPane.setEditable(false);
        queueOutputPane.setContentType("text/html");
        queueOutputPane.setFont(new Font("Segoe UI", Font.PLAIN, 13));
        outputFrame.add(new JScrollPane(queueOutputPane), BorderLayout.CENTER);
        queueOutputPane.setText(queueString);
        outputFrame.setLocationRelativeTo(mainFrame);
        outputFrame.setSize(200, 270);
        return outputFrame;
    }
}
