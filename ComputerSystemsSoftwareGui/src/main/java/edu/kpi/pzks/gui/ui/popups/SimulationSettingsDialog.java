package edu.kpi.pzks.gui.ui.popups;

import edu.kpi.pzks.core.assign.ProcessorSystem;
import edu.kpi.pzks.core.assign.Scheduler;
import edu.kpi.pzks.core.assign.SimulationInfo;
import edu.kpi.pzks.core.assign.impl.Scheduler3Impl;
import edu.kpi.pzks.core.assign.impl.Scheduler4Impl;
import edu.kpi.pzks.core.assign.impl.Scheduler6Impl;
import edu.kpi.pzks.core.model.Graph;
import edu.kpi.pzks.core.model.Links;
import edu.kpi.pzks.core.model.Node;
import edu.kpi.pzks.core.queue.DoubleFactorQueue;
import edu.kpi.pzks.core.queue.Queue;
import edu.kpi.pzks.core.queue.SingleFactorQueue;
import edu.kpi.pzks.core.queue.factors.impl.CriticalPathByNumberOfNodesAscFactorEvaluatorImpl;
import edu.kpi.pzks.core.queue.factors.impl.NodeWeightDescFactorEvaluator;
import edu.kpi.pzks.core.queue.factors.impl.OutLinksFactorEvaluatorImpl;
import edu.kpi.pzks.core.queue.factors.impl.WeightedCriticalFactorEvaluatorImpl;
import edu.kpi.pzks.gui.ui.panels.GanttDiagramPanel;
import edu.kpi.pzks.gui.utils.STRINGS;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Set;
import javax.swing.AbstractAction;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;

/**
 *
 * @author Aloren
 */
public class SimulationSettingsDialog extends javax.swing.JDialog {

    private Graph taskGraph;
    private Graph systemGraph;
    private Scheduler scheduler;
    private Queue queueGenerator;
    private final static String scheduler3 = STRINGS.SCHEDULER + " 3";
    private final static String scheduler4 = STRINGS.SCHEDULER + " 4";
    private final static String scheduler6 = STRINGS.SCHEDULER + " 6";
    private final static String queue1 = STRINGS.QUEUE + " 1";
    private final static String queue8 = STRINGS.QUEUE + " 8";
    private final static String queue12 = STRINGS.QUEUE + " 12";
    private final String[] schedulers = new String[]{scheduler3, scheduler4, scheduler6};

    public SimulationSettingsDialog(JFrame mainFrame, Graph taskGraph, Graph systemGraph) {
        super(mainFrame, STRINGS.MODELING_SETTINGS);
        this.taskGraph = taskGraph;
        this.systemGraph = systemGraph;
        initComponents();
        setResizable(false);
    }

    class SchedulerTypeChangedListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            JComboBox comboBox = (JComboBox) e.getSource();
            switch (comboBox.getSelectedIndex()) {
                case 0:
                    scheduler = new Scheduler3Impl();
                    break;
                case 1:
                    scheduler = new Scheduler4Impl();
                    break;
                case 2:
                    scheduler = new Scheduler6Impl();
                    break;
            }
        }
    }

    class QueueTypeChangedListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            JComboBox comboBox = (JComboBox) e.getSource();
            Set<Node> nodes = taskGraph.getNodes();
            Links links = taskGraph.getLinks();
            switch (comboBox.getSelectedIndex()) {
                case 0: //queue 1
                    queueGenerator = new SingleFactorQueue(new WeightedCriticalFactorEvaluatorImpl(nodes, links), nodes);
                    break;
                case 1: //queue 8
                    queueGenerator = new DoubleFactorQueue(new CriticalPathByNumberOfNodesAscFactorEvaluatorImpl(nodes, links),
                            new NodeWeightDescFactorEvaluator(), nodes);
                    break;
                case 2: //queue 12
                    queueGenerator = new SingleFactorQueue(new OutLinksFactorEvaluatorImpl(), nodes);
                    break;
            }
        }
    }

    class StartSimulationAction extends AbstractAction {

        @Override
        public void actionPerformed(ActionEvent e) {
            ProcessorSystem system = new ProcessorSystem(systemGraph, taskGraph);
            final SimulationInfo.LinkMode isDuplex = SimulationSettingsDialog.this.duplexBox.isSelected()
                    ? SimulationInfo.LinkMode.DUPLEX : SimulationInfo.LinkMode.HALF_DUPLEX;
            final Integer numberOfChannels = (Integer) SimulationSettingsDialog.this.numberOfChannelsSpinner.getValue();
            system.simulationInfo = new SimulationInfo(numberOfChannels, isDuplex, queueGenerator);

            system.scheduler = SimulationSettingsDialog.this.scheduler;

            system.simulation.start();

            ProcessorSystem.Statistics events = system.simulation.collectStatistics();
            GanttDiagramPanel chart = new GanttDiagramPanel(events);

            JFrame chartFrame = new JFrame();
            chartFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            chartFrame.setLocationRelativeTo(SimulationSettingsDialog.this);
            chartFrame.setTitle("Ganntt diagram");
            chartFrame.setAlwaysOnTop(true);
            chartFrame.setVisible(true);
            chartFrame.setContentPane(chart.rootPanel);
            chartFrame.pack();
            SimulationSettingsDialog.this.dispose();
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        startButton = new javax.swing.JButton(new StartSimulationAction());
        cancelButton = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        schedulerBox = new javax.swing.JComboBox();
        jLabel1 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        queueBox = new javax.swing.JComboBox();
        jLabel2 = new javax.swing.JLabel();
        SpinnerModel spinnerModel = new SpinnerNumberModel(1, 1, 20, 1);
        numberOfChannelsSpinner = new javax.swing.JSpinner(spinnerModel);
        jLabel6 = new javax.swing.JLabel();
        duplexBox = new javax.swing.JCheckBox();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        org.openide.awt.Mnemonics.setLocalizedText(startButton, STRINGS.START);

        org.openide.awt.Mnemonics.setLocalizedText(cancelButton, STRINGS.CANCEL);
        cancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelButtonActionPerformed(evt);
            }
        });

        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        schedulerBox.addActionListener(new SchedulerTypeChangedListener());
        schedulerBox.setModel(new javax.swing.DefaultComboBoxModel(schedulers));
        schedulerBox.setSelectedIndex(0);

        org.openide.awt.Mnemonics.setLocalizedText(jLabel1, STRINGS.SCHEDULER+":");

        org.openide.awt.Mnemonics.setLocalizedText(jLabel3, STRINGS.NUM_CHANNELS+":");

        queueBox.addActionListener(new QueueTypeChangedListener());
        queueBox.setModel(new javax.swing.DefaultComboBoxModel(new String[]{this.queue1, queue8, queue12}));
        queueBox.setSelectedIndex(0);

        org.openide.awt.Mnemonics.setLocalizedText(jLabel2, STRINGS.QUEUE+":");

        org.openide.awt.Mnemonics.setLocalizedText(jLabel6, STRINGS.CHANNELS_TYPE+":");

        org.openide.awt.Mnemonics.setLocalizedText(duplexBox, STRINGS.DUPLEX);
        duplexBox.setSelected (true);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel2Layout.createSequentialGroup()
                            .addGap(58, 58, 58)
                            .addComponent(jLabel1))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                            .addContainerGap()
                            .addComponent(jLabel2)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel6)
                            .addComponent(jLabel3))))
                .addGap(34, 34, 34)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(numberOfChannelsSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(duplexBox)
                    .addComponent(schedulerBox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(queueBox, 0, 95, Short.MAX_VALUE))
                .addContainerGap(26, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(schedulerBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(queueBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(numberOfChannelsSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(duplexBox))
                .addContainerGap(25, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(startButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cancelButton)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(startButton)
                    .addComponent(cancelButton))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cancelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelButtonActionPerformed
        this.dispose();
    }//GEN-LAST:event_cancelButtonActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton cancelButton;
    private javax.swing.JCheckBox duplexBox;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JSpinner numberOfChannelsSpinner;
    private javax.swing.JComboBox queueBox;
    private javax.swing.JComboBox schedulerBox;
    private javax.swing.JButton startButton;
    // End of variables declaration//GEN-END:variables
}
