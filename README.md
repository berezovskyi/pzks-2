Computer systems software - II
====

Course project was focused on studying properties of multiprocessor scheduling algorithms with consideration of memory transfers.

GUI allows to graphically input the task graph as well as processor graph. 

![](raw/master/README.images/SNAG-0002.png)

![](raw/master/README.images/SNAG-0003.png)

![](//i.imgur.com/571VwSE.png)

![](//i.imgur.com/XqWjD8i.png)

![](raw/master/README.images/SNAG-0085.png)

![](raw/master/README.images/SNAG-0091.png)

![](raw/master/README.images/SNAG-0086.png)

![](raw/master/README.images/SNAG-0090.png)
